% get all measured contour values
%
% Used for computation of average thicknesses and areas for all nine
% anatomical locations of the circle of Willis, for MR and histology based
% measurements.
%
% KM van Hespen, UMC Utrecht, 2020

clear all
cd('D:\SurfDrive\1. Original Data hypertension\')
[~, ~, manual_edits] = xlsread('Manual_edits_area_thickness.xlsx', '', '');
cd('D:\SurfDrive\1. Original Data hypertension\Contourresults')
allcontourfiles = dir('Contour-contour_distances*.mat');

Hypertensive_pt_names = ['S017'; 'S023'; 'S026'; 'S027'; 'S028'; 'S031'; 'S032'; 'S033'; 'S034'; 'S035'; 'S036'; 'S037'; 'S039'; 'S041'; 'S043'; 'S048'; 'S050'; 'S053'; 'S054'; 'S056'; 'S062'; 'S063'; 'S064'; 'S065'];
% Hypertensive patients when using kidney tissue analysis.
%Hypertensive_pt_names = ['S017'; 'S041'; 'S053'; 'S062'];%(more than half)
%Hypertensive_pt_names2 =  ['S017'; 'S032'; 'S036'; 'S041'; 'S064'; 'S053'; 'S062'];%(Expert)
%Hypertensive_pt_names =  ['S015'; 'S024'; 'S031'; 'S034'; 'S043'; 'S044'; 'S046'; 'S048'; 'S051'; 'S058'; 'S057'; 'S017'; 'S032'; 'S036'; 'S041'; 'S064'; 'S053'; 'S062'];%(More than 3)
 
% Cell containing all patient IDs, ages, and sex
Names_age_gender = {'S015', 'S017', 'S020', 'S021', 'S022', 'S023', 'S024', 'S025', 'S026', 'S027', 'S028', 'S029', 'S030', 'S031', 'S032', 'S033', 'S034', 'S035', 'S036', 'S037', 'S038', 'S039', 'S040', 'S041', 'S042', 'S043', 'S044', 'S045', 'S046', 'S047', 'S048', 'S049', 'S050', 'S051', 'S053', 'S054', 'S055', 'S056', 'S057', 'S058', 'S059', 'S060', 'S061', 'S062', 'S063', 'S064', 'S065', 'S066', 'S067', 'S068';
    56, 78, 62, 86, 62, 72, 68, 58, 64, 64, 80, 82, 74, 74, 71, 74, 87, 69, 68, 64, 53, 72, 66, 55, 72, 61, 56, 65, 70, 80, 65, 87, 54, 71, 60, 87, 84, 68, 58, 63, 67, 71, 54, 62, 63, 69, 64, 64, 65, 77;
    1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1};

Analyse_for_gender_x = 2;    % 0 for female analysis, 1 for male analysis, 2 for all analysis.
doamcaswell = 1;             % Also computes results from AMC histology measurements
dothickness = 1;             % 0, computes area measurements. 1, computes thickness measurements

%% Loads all MR measurements, and sorts them in a cell array.
measurement_results = [];
if dothickness
    y_axis_lim = 0.7;
else
    y_axis_lim = 6.5;
end

for cur_file = 1:numel(allcontourfiles)
    curcontourfile = load(allcontourfiles(cur_file).name);
    allcontourfiles(cur_file).name = strrep(allcontourfiles(cur_file).name, '_new', '');
    subjname = {};
    if ~isempty(find(sum(Hypertensive_pt_names == regexprep(allcontourfiles(cur_file).name(35:39), '_|-', ''), 2) == 4))
        switchht = 1;
    else
        switchht = 0;
    end
    age = Names_age_gender(2, find(strcmp(Names_age_gender(1, :), regexprep(allcontourfiles(cur_file).name(35:39), '_|-', '')) == 1));
    age = age{1};
    gender = Names_age_gender(3, find(strcmp(Names_age_gender(1, :), regexprep(allcontourfiles(cur_file).name(35:39), '_|-', '')) == 1));
    gender = gender{1};
    if cur_file>1
        HTorCT = mat2cell(switchht*ones(1, size(curcontourfile.results, 2)-1), 1, ones(1, size(curcontourfile.results, 2)-1));
        subj_age = mat2cell(age*ones(1, size(curcontourfile.results, 2)-1), 1, ones(1, size(curcontourfile.results, 2)-1));
        subj_gender = mat2cell(gender*ones(1, size(curcontourfile.results, 2)-1), 1, ones(1, size(curcontourfile.results, 2)-1));

        [subjname{1:numel(HTorCT)}] = deal(regexprep(allcontourfiles(cur_file).name(35:39), '_|-', ''));
        measurement_results{cur_file, 1} = {curcontourfile.results(:, 2:end);HTorCT;subjname;subj_age;subj_gender};
        measurement_results{cur_file, 1} = vertcat(measurement_results{cur_file, 1}{:});
        
    else
        HTorCT = mat2cell([0, switchht*ones(1, size(curcontourfile.results, 2)-1)], 1, ones(1, size(curcontourfile.results, 2)));
        subj_age = mat2cell([0, age*ones(1, size(curcontourfile.results, 2)-1)], 1, ones(1, size(curcontourfile.results, 2)));
        subj_gender = mat2cell([0, gender*ones(1, size(curcontourfile.results, 2)-1)], 1, ones(1, size(curcontourfile.results, 2)));
       
        [subjname{1:numel(HTorCT)}] = deal(regexprep(allcontourfiles(cur_file).name(35:39), '_|-', ''));
        measurement_results{cur_file, 1} = {curcontourfile.results;HTorCT;subjname;subj_age;subj_gender};
        measurement_results{cur_file, 1} =  vertcat(measurement_results{cur_file, 1}{:});
    end
end

measurement_results = horzcat(measurement_results{:});
measurement_results{15, 1} = 'HTorCT';
measurement_results{16, 1} = 'pt_name';
measurement_results{17, 1} = 'age';
measurement_results{18, 1} = 'MaleorFemale';

quantile25 = [];
quantile75 = [];
maleorfemale = measurement_results(end, :)';
FWHM_thresholds = measurement_results(14, :)';
Hausdorff_distance =  measurement_results(12, :)';

% Filters out measurements where the contours were drawn outside 0.479 and
% 0.521 background to foreground intensity, given that in general the FWHM 
% is drawn at 0.5 background to foreground intensity 
if Analyse_for_gender_x<2
    measurement_results_selection =  [1;cell2mat(FWHM_thresholds(2:end))<0.521 & cell2mat(FWHM_thresholds(2:end))>0.479 & cell2mat(maleorfemale(2:end)) == Analyse_for_gender_x];
else
    measurement_results_selection =  [1;cell2mat(FWHM_thresholds(2:end))<0.521 & cell2mat(FWHM_thresholds(2:end))>0.479 & cell2mat(maleorfemale(2:end))<3];

end
thresholded_measurement_results = [];

for ind = 1:numel(FWHM_thresholds)
    if measurement_results_selection(ind)
        thresholded_measurement_results{end+1} =  {measurement_results{:, ind}}';
    end
end
thresholded_measurement_results = horzcat(thresholded_measurement_results{:});


%% New AMC processing script
if doamcaswell
    %Creates an overcomplete results cell array, where regardless if
    %measurements are taken for a certain location, a column is created for
    %each location and specimen.
    
    Overcomplete_AMC_UMC_comp_results = []
    amcstructnames = {'A1'; 'A2'; 'A3'; 'A4'; 'A5'; 'A6'; 'B1'; 'B2'; 'B3'; 'B4'; 'B5'; 'C1'; 'C2'; 'C3'; 'C4'; 'C5'; 'C6'; 'D1'; 'D2'; 'D3'; 'D4'}';
    subjnameorder = {'S015'; 'S017'; 'S020'; 'S021'; 'S022'; 'S023'; 'S024'; 'S025'; 'S026'; 'S027'; 'S028'; 'S029'; 'S030'; 'S031'; 'S032'; 'S033'; 'S034'; 'S035'; 'S036'; 'S037'; 'S038'; 'S039'; 'S040'; 'S041'; 'S042'; 'S043'; 'S044'; 'S045'; 'S046'; 'S047'; 'S048'; 'S049'; 'S050'; 'S051'; 'S053'; 'S054'; 'S055'; 'S056'; 'S057'; 'S058'; 'S059'; 'S060'; 'S061'; 'S062'; 'S063'; 'S064'; 'S065'; 'S066'; 'S067'; 'S068'}';
    load('C:\Users\khespen\Documents\5. Hypertension\amc_final_final.mat')
    count_zeros_amc = zeros(22, 1);
    count_zeros = zeros(22, 1);
    concatnamesandsubj_amc = cellfun(@(a, b) {strcat(a, b)}, (amc_final(:, 1)), amc_final(:, 2));
    concatnamesandsubj = cellfun(@(a, b) {strcat(a, b)}, (thresholded_measurement_results(16, :)), thresholded_measurement_results(1, :));
    
    ind = 1;
    Overcomplete_AMC_UMC_comp_results = zeros(2, numel(amc_final));
    Overcomplete_AMC_UMC_comp_results = {'Location'; 'S-number'; 'UMC'; 'AMC'; 'HTorCT'; 'Male'};
    AMC_UMC_comp_results_deleted_zero_measurements = {'Location'; 'S-number'; 'UMC'; 'AMC'; 'HTorCT'; 'Male'};
    unmeasured = {''};
    % Tries to sort all results in the correct column of the overcomplete
    % cell array, for both the AMC and UMC measurements.
    for rows = subjnameorder 
        for cols = amcstructnames
            Index = [];
            Index_amc = [];
            ind = ind+1;
            curname = strcat(rows{1}, cols{1});
            curname_swapped = strcat(cols{1}, rows{1});
            %UMC            
            Index = find(contains(concatnamesandsubj, curname));
            Overcomplete_AMC_UMC_comp_results{1, ind} = cols;
            Overcomplete_AMC_UMC_comp_results{2, ind} = rows;
            AMC_UMC_comp_results_deleted_zero_measurements{1, ind} = cols;
            AMC_UMC_comp_results_deleted_zero_measurements{2, ind} = rows;
            if Index 
                if ~dothickness
                    Overcomplete_AMC_UMC_comp_results{3, ind} =  mean(cell2mat(thresholded_measurement_results(13, Index))-cell2mat(thresholded_measurement_results(12, Index)));
                else
                    Overcomplete_AMC_UMC_comp_results{3, ind} = (cell2mat(thresholded_measurement_results(3, Index))+cell2mat(thresholded_measurement_results(2, Index)))/2;
                end
            else
                Overcomplete_AMC_UMC_comp_results{3, ind} = 0;
            end
            %manual_edits
            Index_location = find(contains(amcstructnames, cols));
            Index_edits  = [];
            Index_edits = find(contains({manual_edits{:, 1}}, curname_swapped));
            if Index_edits
                area_thick_vals = strsplit(manual_edits{Index_edits, 2}, '/');
                
                Overcomplete_AMC_UMC_comp_results{3, ind} = str2num(area_thick_vals{dothickness+1});
            end
            
            %AMC
            Index_amc = find(contains(concatnamesandsubj_amc, curname));
            if Index_amc
                if  amc_final{Index_amc, 3}
                    % Properly scales the AMC measurements in mm and mm2.
                    if ~dothickness
                        Overcomplete_AMC_UMC_comp_results{4, ind} = amc_final{Index_amc, 4}/1000000;
                    else
                        Overcomplete_AMC_UMC_comp_results{4, ind} = amc_final{Index_amc, 3}/1000;
                    end
                else
                    Overcomplete_AMC_UMC_comp_results{4, ind} = 0;
                end
            else
                Overcomplete_AMC_UMC_comp_results{4, ind} = 0;
            end
    
            if isempty(Overcomplete_AMC_UMC_comp_results{4, ind})
                Overcomplete_AMC_UMC_comp_results{4, ind} = 0;
            end
            if Overcomplete_AMC_UMC_comp_results{3, ind} ==  0 &  ~Overcomplete_AMC_UMC_comp_results{4, ind} ==  0
                count_zeros(Index_location) =  count_zeros(Index_location)+1;
                unmeasured{end+1, :} = strcat(curname, '-UMC');
            end
            if Overcomplete_AMC_UMC_comp_results{4, ind} ==  0 & ~Overcomplete_AMC_UMC_comp_results{3, ind} ==  0
                count_zeros_amc(Index_location) =  count_zeros_amc(Index_location)+1;
                unmeasured{end+1, :} = strcat(curname, '-amc');
                
            end           
            if 0 & Overcomplete_AMC_UMC_comp_results{4, ind} ==  0 &~ Overcomplete_AMC_UMC_comp_results{3, ind} ==  0
                count_zeros_amc(Index_location) =  count_zeros_amc(Index_location)+1;
                unmeasured{end+1, :} = strcat(curname, '-amc');
            end
            Overcomplete_AMC_UMC_comp_results{5, ind} = sum(contains(Hypertensive_pt_names, rows));
            Overcomplete_AMC_UMC_comp_results{6, ind} = Names_age_gender{3, find(contains({Names_age_gender{1, :}}, rows))};

            if ~Overcomplete_AMC_UMC_comp_results{4, ind} ==  0 & ~Overcomplete_AMC_UMC_comp_results{3, ind} == 0
                if ~dothickness
                    AMC_UMC_comp_results_deleted_zero_measurements{3, ind} =  mean(cell2mat(thresholded_measurement_results(13, Index))-cell2mat(thresholded_measurement_results(12, Index)));
                else
                    AMC_UMC_comp_results_deleted_zero_measurements{3, ind} = (cell2mat(thresholded_measurement_results(3, Index))+cell2mat(thresholded_measurement_results(2, Index)))/2;
                end
                
                if Index_edits
                    area_thick_vals = strsplit(manual_edits{Index_edits, 2}, '/');
                    AMC_UMC_comp_results_deleted_zero_measurements{3, ind} = str2num(area_thick_vals{dothickness+1});
                end
                
                if ~dothickness
                    AMC_UMC_comp_results_deleted_zero_measurements{4, ind} = amc_final{Index_amc, 4}/1000000;
                else
                    AMC_UMC_comp_results_deleted_zero_measurements{4, ind} = amc_final{Index_amc, 3}/1000;
                end
                
                AMC_UMC_comp_results_deleted_zero_measurements{5, ind} = sum(contains(Hypertensive_pt_names, rows))+sum(contains(Hypertensive_pt_names, rows));
                AMC_UMC_comp_results_deleted_zero_measurements{6, ind} = Names_age_gender{3, find(contains({Names_age_gender{1, :}}, rows))};
            end
            
            
        end
    end

end
% removes all excess columns where no measurements are present for both measurement methods.    
AMC_UMC_comp_results_deleted_zero_measurements(:, any(cellfun(@isempty, AMC_UMC_comp_results_deleted_zero_measurements), 1)) = [];

comparison_array = [cell2mat({Overcomplete_AMC_UMC_comp_results{3, 2:end}});cell2mat({Overcomplete_AMC_UMC_comp_results{4, 2:end}})];
comparison_array( :, ~all(comparison_array, 1) ) = [];
UMC = comparison_array(1, :);
AMC = comparison_array(2, :);

%bland altmann
BlandAltman(AMC', UMC', '', '', '', 'axesLimits', 'auto', 'colors', lines, 'symbols', 'o', 'markerSize', 6)
%scatter((comparison_array(2, :)+comparison_array(1, :))/2, comparison_array(1, :)-comparison_array(2, :))
[rpc fig] = BlandAltman(AMC', UMC', '', '', '', 'axesLimits', 'auto', 'colors', lines, 'symbols', 'o', 'markerSize', 6)
BAplot = fig.Children(1);
set(BAplot, 'LineWidth', 1, 'FontSize', 14)
BAplot.Children(1).String{1} = '';
BAplot.Children(1).String{2} = '';
BAplot.Children(2).FontSize = 12;
BAplot.Children(3).FontSize = 12;
BAplot.Children(4).FontSize = 12;
BAplot.Children(5).LineWidth = 2;
BAplot.Children(6).LineWidth = 2;
BAplot.Children(7).LineWidth = 2;
BAplot.Children(8).LineWidth = 2;
BAplot.XLabel.String = 'Mean total wall area MRI and histology (mm2)';
BAplot.YLabel.String = '\Delta total wall area MRI and histology (mm2)';

if 1
    pointplot =  subplot(1, 2, 1)
    pointplot.Position = [0.275 0.252 0.258 0.517]
    scatter(AMC, UMC, 'LineWidth', 1)
    ylabel('Total wall area MRI (mm2)');
    xlabel('Total wall area histology w/o adventitia (mm2)');
    box on;
    set(gca, 'LineWidth', 2, 'FontSize', 14)
end
%scatter
%figure;
%scatter(comparison_array(1, :), comparison_array(2, :))
%comparison_precleaned = comparison;
% get average thicknesses or areas
structurenames = string(AMC_UMC_comp_results_deleted_zero_measurements(1, 2:end));
ptnames = string(AMC_UMC_comp_results_deleted_zero_measurements(2, 2:end));
mean_UMC = cell2mat(AMC_UMC_comp_results_deleted_zero_measurements(3, 2:end));
%meanthick = (cell2mat(cleanedcontourresults(10, 2:end))/(2*pi)).^2*pi;
mean_AMC = cell2mat(AMC_UMC_comp_results_deleted_zero_measurements(4, 2:end));
%meanthick = meanthick./meanarea;
ishtorct = cell2mat(AMC_UMC_comp_results_deleted_zero_measurements(end-1, 2:end));
MF = cell2mat(AMC_UMC_comp_results_deleted_zero_measurements(end, 2:end));

% Splits measurements based on hypertension status, and if selected by
% gender
if Analyse_for_gender_x<2
    structurenamesHT = structurenames(ishtorct == 1 & MF ==  Analyse_for_gender_x);
    mean_HT_UMC = mean_UMC(ishtorct == 1 & MF ==  Analyse_for_gender_x);
    mean_HT_AMC = mean_AMC(ishtorct == 1 &MF ==  Analyse_for_gender_x);
    ptnamesht = ptnames(ishtorct == 1 & MF ==  Analyse_for_gender_x);
    
    structurenamesCT = structurenames(ishtorct == 0 & MF ==  Analyse_for_gender_x);
    mean_CT_UMC = mean_UMC(ishtorct == 0 & MF ==  Analyse_for_gender_x);
    mean_CT_AMC = mean_AMC(ishtorct == 0 & MF ==  Analyse_for_gender_x);
else
    structurenamesHT = structurenames(ishtorct>0 & MF < 3);
    mean_HT_UMC = mean_UMC(ishtorct>0 & MF < 3);
    mean_HT_AMC = mean_AMC(ishtorct>0 & MF < 3);
    ptnamesct = ptnames(ishtorct == 0 & MF < 3);
    ptnamesht = ptnames(ishtorct>0 & MF < 3);
    structurenamesCT = structurenames(ishtorct == 0 & MF < 3);
    mean_CT_UMC = mean_UMC(ishtorct == 0 & MF < 3);
    mean_CT_AMC = mean_AMC(ishtorct == 0 & MF < 3);
end
% Output array used for SPSS analysis.
SPSS_array = AMC_UMC_comp_results_deleted_zero_measurements';


%% Following script is used for visualization of measurements.
% VA = A1, A2
% BA = A3, A6, B1
% AICA = A4, A5
% ICA = C5, C6
% MCA = C2, C4
% ACA = D1, D2, C1, C3
% PCoA = B4, B5
% PCA = B2, B3
% SCA = D3, D4
AMC_averaged = {0, 0, 0, 0, 0, 0};
% sort thicknesses hypertension based on structures
fillcellht = {[];[];[];[];[];[];[];[];[]};
index_pt = 1;
% All measurements are sorted per location, and if more measurements exist
% for a location and specimen, the average is taken.
for istruct = 1:numel(structurenamesHT)
    curstruct = structurenamesHT(istruct);
    if any(curstruct == 'A1' | curstruct == 'A2')
        col = 1;
        name = 'VA';
    elseif any(curstruct == 'B1' |curstruct == 'A3' | curstruct == 'A6')
        col = 2;  
        name = 'BA';
    elseif any(curstruct == 'A4' | curstruct == 'A5')
        col = 9;
        name = 'AICA';
    elseif any(curstruct == 'C5' | curstruct == 'C6')
        col = 3;
        name = 'ICA';
    elseif any(curstruct == 'C2' | curstruct == 'C4')
        col = 4;
        name = 'MCA';
    elseif any(curstruct == 'D1' | curstruct == 'D2' | curstruct == 'C1' | curstruct == 'C3')
        col = 5;
        name = 'ACA';
    elseif any(curstruct == 'B4' | curstruct == 'B5')
        col = 6;
        name = 'PCoA';
    elseif any( curstruct == 'B2'| curstruct == 'B3')
        col = 7;
        name = 'PCA';
    elseif any(curstruct == 'D3' | curstruct == 'D4')
        col = 8;
        name = 'SCA';

    end
    if col<10
    if any(cellfun(@(s) strcmp(name+ptnamesht(istruct), s), AMC_averaged(:, 3)))
       addindex = find(cellfun(@(s) strcmp(name+ptnamesht(istruct), s), AMC_averaged(:, 3)));
        AMC_averaged{addindex , 1} = mean_HT_AMC(istruct)+AMC_averaged{addindex , 1};
        AMC_averaged{addindex , 2} = mean_HT_UMC(istruct)+AMC_averaged{addindex , 2};
        AMC_averaged{addindex, 3} = name+ptnamesht(istruct);
        AMC_averaged{addindex, 4} = AMC_averaged{addindex , 4} +1;
        AMC_averaged{addindex, 5} = col;
        AMC_averaged{addindex, 6} = 1+sum(contains(Hypertensive_pt_names, ptnamesht(istruct))) ;
    else
        AMC_averaged{index_pt, 1} = mean_HT_AMC(istruct);
        AMC_averaged{index_pt, 2} = mean_HT_UMC(istruct);
        AMC_averaged{index_pt, 3} = name+ptnamesht(istruct);
        AMC_averaged{index_pt, 4} = 1;
        AMC_averaged{index_pt, 5} = col;
        AMC_averaged{index_pt, 6} = 1+sum(contains(Hypertensive_pt_names, ptnamesht(istruct)));
        index_pt = index_pt+1;
    end   
    end
  %  idx = sum(~cellfun(@isempty, {fillcellht{col, 1, :}}), 2);
  %  fillcellht{col, 1, idx+1} = mean_HT_AMC(istruct);
  %  fillcellht{col, 2, idx+1} = mean_HT_UMC(istruct);
end
% sort thicknesses controls based on structures  
fillcellct = {[];[];[];[];[];[];[];[];[]};
for istruct = 1:numel(structurenamesCT)
    curstruct = structurenamesCT(istruct);
    if any(curstruct == 'A1' | curstruct == 'A2')
        col = 1;
        name = 'VA';
    elseif any(curstruct == 'B1' |curstruct == 'A3' | curstruct == 'A6')
        col = 2;  
        name = 'BA';
    elseif any(curstruct == 'A4' | curstruct == 'A5')
        col = 9;
        name = 'AICA';
    elseif any(curstruct == 'C5' | curstruct == 'C6')
        col = 3;
        name = 'ICA';
    elseif any(curstruct == 'C2' | curstruct == 'C4')
        col = 4;
        name = 'MCA';
    elseif any(curstruct == 'D1' | curstruct == 'D2' | curstruct == 'C1' | curstruct == 'C3')
        col = 5;
        name = 'ACA';
    elseif any(curstruct == 'B4' | curstruct == 'B5')
        col = 6;
        name = 'PCoA';
    elseif any( curstruct == 'B2'| curstruct == 'B3')
        col = 7;
        name = 'PCA';
    elseif any(curstruct == 'D3' | curstruct == 'D4')
        col = 8;
        name = 'SCA';
    end
    if col<10
        if any(cellfun(@(s) strcmp(name+ptnamesct(istruct), s), AMC_averaged(:, 3)))
            addindex = find(cellfun(@(s) strcmp(name+ptnamesct(istruct), s), AMC_averaged(:, 3)));
            AMC_averaged{addindex , 1} = mean_CT_AMC(istruct)+AMC_averaged{addindex , 1};
            AMC_averaged{addindex , 2} = mean_CT_UMC(istruct)+AMC_averaged{addindex , 2};
            AMC_averaged{addindex, 3} = name+ptnamesct(istruct);
            AMC_averaged{addindex, 4} = AMC_averaged{addindex , 4} +1;
            AMC_averaged{addindex, 5} = col;
            AMC_averaged{addindex, 6} = 0;
        else
            AMC_averaged{index_pt, 1} = mean_CT_AMC(istruct);
            AMC_averaged{index_pt, 2} = mean_CT_UMC(istruct);
            AMC_averaged{index_pt, 3} = name+ptnamesct(istruct);
            AMC_averaged{index_pt, 4} = 1;
            AMC_averaged{index_pt, 5} = col;
            AMC_averaged{index_pt, 6} = 0;
            index_pt = index_pt+1;
        end   
    end
    % idx = sum(~cellfun(@isempty, {fillcellct{col, 1, :}}), 2);
    % fillcellct{col, 1, idx+1} = mean_CT_AMC(istruct);
    % fillcellct{col, 2, idx+1} = mean_CT_UMC(istruct);
end

AMC_averaged(:, 1) =  num2cell(cell2mat(AMC_averaged(:, 1))./cell2mat(AMC_averaged(:, 4))) ;
AMC_averaged(:, 2) =  num2cell(cell2mat(AMC_averaged(:, 2))./cell2mat(AMC_averaged(:, 4)));
[rpc fig] =  BlandAltman(cell2mat(AMC_averaged(:, 1)), cell2mat(AMC_averaged(:, 2)), '', '', '', 'axesLimits', 'auto', 'colors', lines, 'symbols', 'o', 'markerSize', 6)
BAplot = fig.Children(1);
set(BAplot, 'LineWidth', 2, 'FontSize', 14)
BAplot.Children(1).String{1} = '';
BAplot.Children(1).String{2} = '';
BAplot.Children(2).FontSize = 12;
BAplot.Children(3).FontSize = 12;
BAplot.Children(4).FontSize = 12;
BAplot.Children(5).LineWidth = 2;
BAplot.Children(6).LineWidth = 2;
BAplot.Children(7).LineWidth = 2;
BAplot.Children(8).LineWidth = 2;
BAplot.XLabel.String = 'Mean wall thickness MRI and histology (mm)';
BAplot.YLabel.String = '\Delta wall thickness MRI and histology (mm)';
if 1
    pointplot =  subplot(1, 2, 1)
    pointplot.Position = [0.275 0.252 0.258 0.517]
    scatter(cell2mat(AMC_averaged(:, 1)), cell2mat(AMC_averaged(:, 2)), 'LineWidth', 2)
    ylabel('Wall thickness MRI (mm)');
    xlabel('Wall thickness histology (mm)');
    box on;
    set(gca, 'LineWidth', 2, 'FontSize', 14)
end

for curav = 1: size(AMC_averaged, 1)
    if AMC_averaged{curav, 6} == 0
        col = (AMC_averaged{curav, 5});
        idx = sum(~cellfun(@isempty, {fillcellct{col, 1, :}}), 2);
        fillcellct{col, 1, idx+1} = AMC_averaged{curav, 1};
        fillcellct{col, 2, idx+1} = AMC_averaged{curav, 2};
    else
        col = AMC_averaged{curav, 5};
        idx = sum(~cellfun(@isempty, {fillcellht{col, 1, :}}), 2);
        fillcellht{col, 1, idx+1} = AMC_averaged{curav, 1};
        fillcellht{col, 2, idx+1} = AMC_averaged{curav, 2};
    end
end
%%
% Prepare the sorted thickness/ area measurements for visualization
matht = cellfun(@(x) mean(x(:, :, 1)), fillcellht);
matct = cellfun(@(x) mean(x(:, :, 1)), fillcellct);
matht(isnan(matht)) = 0;
matct(isnan(matct)) = 0;
stats = [];
quantile25ct = [];
midquantile = [];
midquantilect = [];
lengthbar = [];
lengthbarct = [];
quantile75ct = [];
normalize = 1;
permatct = permute(matct, [3, 1, 2]);
permatht = permute(matht, [3, 1, 2]);

% Compute median and IQRs
for idx = 1:9
    for idy = 1:2
        %normalize = median(nonzeros(matct(idx, idy, :)));
        quantile25 = quantile(nonzeros(matht(idx, idy, :))/normalize, 0.25);
        quantile75 = quantile(nonzeros(matht(idx, idy, :))/normalize, 0.75);
        resultsmatHT(idx, idy, 1) = median(nonzeros(matht(idx, idy, :))/normalize);
        resultsmatHT(idx, idy, 2) = iqr(nonzeros(matht(idx, idy, :))/normalize)/2;
        
        resultsmatCT(idx, idy, 1) = median(nonzeros(matct(idx, idy, :))/normalize);
        resultsmatCT(idx, idy, 2) = iqr(nonzeros(matct(idx, idy, :))/normalize)/2;
        stats(idx, idy) = ranksum(nonzeros(matht(idx, idy, :)), nonzeros(matct(idx, idy, :)));
        quantile25ct = quantile(nonzeros(matct(idx, idy, :))/normalize, 0.25);
        quantile75ct = quantile(nonzeros(matct(idx, idy, :))/normalize, 0.75);
        midquantile(idx, idy, 1) = [(quantile75+quantile25)/2];
        lengthbar(idx, idy, 1) =  quantile75-quantile25;
        midquantilect(idx, idy, 1) = [(quantile75ct+quantile25ct)/2];
        lengthbarct(idx, idy, 1) =  quantile75ct-quantile25ct;
    end
end


% Plot  results
names = {'VA'; 'BA'; 'ICA'; 'MCA'; 'ACA'; 'PCoA'; 'PCA'};
figure;
pr = get(gca, 'colororder');

hold on; box on
bar(linspace(1, 27, 7), resultsmatHT(1:end-2, 2, 1), 0.2, 'FaceColor', pr(1, :), 'linewidth', 2, 'EdgeColor', 'k')
bar(linspace(2, 28, 7), resultsmatCT(1:end-2, 2, 1), 0.2, 'FaceColor', pr(2, :), 'linewidth', 2, 'EdgeColor', 'k')
errorbar(linspace(1, 27, 7), midquantile(1:end-2, 2)...
 , lengthbar(1:end-2, 2)/2, 'LineStyle', 'none', 'marker', 'none', 'MarkerSize', 8, 'Color', [0.83, 0.82, 0.78], 'linewidth', 2)
errorbar(linspace(2, 28, 7), midquantilect(1:end-2, 2)...
 , lengthbarct(1:end-2, 2)/2, 'LineStyle', 'none', 'marker', 'none', 'MarkerSize', 8, 'Color', [0.83, 0.82, 0.78], 'linewidth', 2)
if dothickness
    ylabel('Vessel wall thickness (mm)')
else
    ylabel('Vessel wall area (mm^2)')
end

set(gcf, 'color', 'w')
set(gca, 'xtick', linspace(1.5, 27.5, 7), 'xticklabel', names, 'FontSize', 24, 'linewidth', 2, 'Color', 'w', 'Zcolor', 'k', 'Xcolor', 'k', 'Ycolor', 'k')
leg = legend('Hypertension', 'Control', 'Location', 'northeastoutside');
set(leg, 'TextColor', 'k', 'EdgeColor', 'k', 'FontSize', 24)
ylim([0 y_axis_lim])
hold off

figure;
if dothickness
    ylabel('Vessel wall thickness (mm)')
else
    ylabel('Vessel wall area (mm^2)')
end
hold on; box on
bar(linspace(1, 27, 7), resultsmatHT(1:end-2, 1, 1), 0.2, 'FaceColor', pr(1, :), 'linewidth', 2, 'EdgeColor', 'k')
bar(linspace(2, 28, 7), resultsmatCT(1:end-2, 1, 1), 0.2, 'FaceColor', pr(2, :), 'linewidth', 2, 'EdgeColor', 'k')

errorbar(linspace(1, 27, 7), midquantile(1:end-2, 1)...
 , lengthbar(1:end-2, 1)/2, 'LineStyle', 'none', 'marker', 'none', 'MarkerSize', 8, 'Color', [0.83, 0.82, 0.78], 'linewidth', 2)
errorbar(linspace(2, 28, 7), midquantilect(1:end-2, 1)...
 , lengthbarct(1:end-2, 1)/2, 'LineStyle', 'none', 'marker', 'none', 'MarkerSize', 8, 'Color', [0.83, 0.82, 0.78], 'linewidth', 2)

set(gcf, 'color', 'w')
set(gca, 'xtick', linspace(1.5, 27.5, 7), 'xticklabel', names, 'FontSize', 24, 'linewidth', 2, 'Color', 'w', 'Zcolor', 'k', 'Xcolor', 'k', 'Ycolor', 'k')
ylim([0 y_axis_lim])
leg = legend('Hypertension', 'Control', 'Location', 'northeastoutside');
set(leg, 'TextColor', 'k', 'EdgeColor', 'k', 'FontSize', 24)
hold off    