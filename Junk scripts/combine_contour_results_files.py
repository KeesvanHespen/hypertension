# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 11:58:10 2018

@author: khespen
"""
import numpy as np
import os

path = 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/22022018_S033_S034/S033'
file_one = 'Contour_results_ROI_0017-S033-34_biascorrected2_2018-05-02_redonefew'
file_two = 'Contour_results_ROI_0017-S033-34_biascorrected2_2018-10-16additional3_pt1'

results=np.load(os.path.join(path,file_one+'.npy'),encoding='latin1')
results2=np.load(os.path.join(path,file_two+'.npy'),encoding='latin1')
for nam in results2[0,1:]:
     val=1
     arr_index = np.where(results[0,:] == nam)[0]   
     if arr_index.size>1:   
         print('checken')
         val=0
         break
         
     
     if arr_index.size==0:
         results=np.concatenate((results,results2[:,np.where(results2[0,:]==nam)[0]]),axis=1)
         print('added '+nam)
     else: 
         arr_index = arr_index[0]
         results[:,arr_index]=results2[:,np.where(results2[0,:]==nam)[0][0]]
         print('changed '+nam)


if val==1:
    np.save(os.path.join(path,file_one+'_combined3.npy'),results)