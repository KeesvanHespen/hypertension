circumference = 10;
radius_inner_circle = 5;
radius_outer_circle = 10;
if 0
    wall_area = 2;
    points = linspace(1,20,50);
    for u = 1:numel(points)
     [r_area,r_peri]=calculateA(1,points(u));
     rsav(u,1) = r_area;
     rsav(u,2) = r_peri;
    end
    plot(linspace(1,20,size(rsav,1)),rsav(:,1)/rsav(1,1)); hold on; plot(linspace(1,20,size(rsav,1)),rsav(:,2)/rsav(1,2)); hold off;
    xlabel('Aspect ratio'); ylabel('Apparent normalized thickness'); legend('Circle calculated with area', 'Circle calculated with perimeter')
end

thickness=1;
hold on;

wall_area = 2;
points = linspace(1.0,4.6,50);
for u = 1:numel(points)
     [r_area,r_peri]=constantWALLAREA(1.9,7.4,points(u));
     rsavconsarea(u,1) = r_area;
     rsavconsarea(u,2) = r_peri;
end

plot(linspace(1,4.4,size(rsavconsarea,1)),rsavconsarea(:,1)./rsavconsarea(1,1)); %hold on; plot(linspace(1,20,size(rsavconsarea,1)),rsavconsarea(:,2)); hold off;  %plot(linspace(1,20,size(rsavconsarea,1)),rsavconsarea(:,2)./rsavconsarea(:,1)); hold off;
xlabel('Aspect ratio'); ylabel('Apparent normalized thickness'); 

function [vessel_wall_thickness_via_area,vessel_wall_thickness_via_peri] = calculateA(ellipse_wall_area,ratio_div_AB)
area_outer = area_ellipse(ratio_div_AB,1); % outer area

area_inner = area_outer-ellipse_wall_area; % lumen inner area
inner_b = sqrt(area_inner/(pi*ratio_div_AB));

perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);
perim_outer = ellipsePerimeter(ratio_div_AB,1);
vessel_wall_thickness_via_area = sqrt(area_outer/pi)-sqrt(area_inner/pi);
vessel_wall_thickness_via_peri = perim_outer/(2*pi)-perim_inner/(2*pi);
end

function [vessel_wall_thickness_via_area,vessel_wall_thickness_via_peri] = constantAREA(area_inner,area_outer, ratio_div_AB)

%area_outer = pi*(thickness+sqrt(area_inner/pi))^2; % outer area
inner_b = sqrt(area_inner/(pi*ratio_div_AB));
outer_b = sqrt(area_outer/(pi*ratio_div_AB));
area_outer_ellipse = area_ellipse(ratio_div_AB*outer_b,outer_b);
area_inner_ellipse = area_ellipse(ratio_div_AB*inner_b,inner_b);
inner_b = sqrt(area_inner/(pi*ratio_div_AB));

perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);
perim_outer = ellipsePerimeter(ratio_div_AB,1);
vessel_wall_thickness_via_area = sqrt(area_outer/pi)-sqrt(area_inner/pi); % assume that your ellipse area is a circle area.
vessel_wall_thickness_via_peri = perim_outer/(2*pi)-perim_inner/(2*pi);
end

function [vessel_wall_thickness_via_area,vessel_wall_thickness_via_peri] = constantWALLAREA(wall_area, perimeter_outer, ratio_div_AB)
b_outer = b_approx(perimeter_outer,ratio_div_AB);
area_outer_ellipse = area_ellipse(ratio_div_AB*b_outer,b_outer)
area_inner_ellipse = area_outer_ellipse-wall_area;
%area_outer = pi*(thickness+sqrt(area_inner/pi))^2; % outer area
inner_b = sqrt(area_inner_ellipse/(pi*ratio_div_AB));
perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);
%area_outer_ellipse = area_ellipse(ratio_div_AB*outer_b,outer_b);
%area_inner_ellipse = area_ellipse(ratio_div_AB*inner_b,inner_b);
%inner_b = sqrt(area_inner/(pi*ratio_div_AB));
%perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);
%perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);

vessel_wall_thickness_via_area = sqrt(area_outer_ellipse/pi)-sqrt(area_inner_ellipse/pi); % assume that your ellipse area is a circle area.
vessel_wall_thickness_via_peri = perimeter_outer/(2*pi)-perim_inner/(2*pi);
end

function [vessel_wall_thickness_via_area,vessel_wall_thickness_via_peri] = constantPERIMETER(perimeter_inner,perimeter_outer,ratio_div_AB)
b_inner = b_approx(perimeter_inner,ratio_div_AB);
area_inner_ellipse = area_ellipse(ratio_div_AB*b_inner,b_inner);
b_outer = b_approx(perimeter_outer,ratio_div_AB);
area_outer_ellipse = area_ellipse(ratio_div_AB*b_outer,b_outer);
vw_area = area_outer_ellipse-area_inner_ellipse;
vessel_wall_thickness_via_area = sqrt(area_outer_ellipse/pi)-sqrt(area_inner_ellipse/pi);
vessel_wall_thickness_via_peri = perimeter_outer/(2*pi)-perimeter_inner/(2*pi);
if 0
    b_inner = b_approx(perimeter_inner,ratio_div_AB);
    area_inner = area_ellipse(ratio_div_AB*b_inner,b_inner);

    inner_radius = sqrt(area_inner/pi);
    outer_radius = thickness+inner_radius;
    outer_area = pi*outer_radius^2; % er van uitgaande dat de perimeters constant zijn, 
                                    % en dat je contouren cirkels zijn, moet outer_area de 
                                    % oppervlakte van het buiten contour zijn om deze bep. dikte te meten.

    outer_perimeter = 2*pi*outer_radius;  % de perimeter van de cirkel.

    b_outer = b_approx(outer_perimeter,ratio_div_AB); % de minor as lengte b, gegeven de perimeter en de aspect ratio.
    outer_area2 = area_ellipse(ratio_div_AB*b_outer,b_outer); % het werkelijke oppervlak van de ellipsoid.
    outer_radius2 = sqrt(outer_area2/pi);
    thicknesstemp = outer_radius2 - inner_radius
    % het wand oppervlak waarbij de omtrekken van inner and outer contour
    % gelijk zijn ligt lager dan wanneer we uitgaan van een cirkel. Dus als
    % perimeters worden gemeten
    inner_b = sqrt(area_inner/(pi*ratio_div_AB));
    perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);

end

end 
function [vessel_wall_thickness_via_area,thickness] = constantTHICK(thickness,ratio_div_AB)
b_inner = b_approx(perimeter_inner,ratio_div_AB);
area_inner = area_ellipse(ratio_div_AB*b_inner,b_inner);
b_outer = b_approx(perimeter_outer,ratio_div_AB);
area_outer = area_ellipse(ratio_div_AB*b_outer,b_outer);
vessel_wall_thickness_via_area = sqrt(area_outer/pi)-sqrt(area_inner/pi);
if 0
    b_inner = b_approx(perimeter_inner,ratio_div_AB);
    area_inner = area_ellipse(ratio_div_AB*b_inner,b_inner);

    inner_radius = sqrt(area_inner/pi);
    outer_radius = thickness+inner_radius;
    outer_area = pi*outer_radius^2; % er van uitgaande dat de perimeters constant zijn, 
                                    % en dat je contouren cirkels zijn, moet outer_area de 
                                    % oppervlakte van het buiten contour zijn om deze bep. dikte te meten.

    outer_perimeter = 2*pi*outer_radius;  % de perimeter van de cirkel.

    b_outer = b_approx(outer_perimeter,ratio_div_AB); % de minor as lengte b, gegeven de perimeter en de aspect ratio.
    outer_area2 = area_ellipse(ratio_div_AB*b_outer,b_outer); % het werkelijke oppervlak van de ellipsoid.
    outer_radius2 = sqrt(outer_area2/pi);
    thicknesstemp = outer_radius2 - inner_radius
    % het wand oppervlak waarbij de omtrekken van inner and outer contour
    % gelijk zijn ligt lager dan wanneer we uitgaan van een cirkel. Dus als
    % perimeters worden gemeten
    inner_b = sqrt(area_inner/(pi*ratio_div_AB));
    perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);

end
%area_outer = pi*(thickness+sqrt(area_inner/pi))^2; % outer area

%outer_b = sqrt(area_outer/(pi*ratio_div_AB));
%area_outer_ellipse = area_ellipse(ratio_div_AB*outer_b,outer_b)
%area_inner_ellipse = area_ellipse(ratio_div_AB*inner_b,inner_b)
%inner_b = sqrt(area_inner/(pi*ratio_div_AB));

%perim_inner = ellipsePerimeter(ratio_div_AB*inner_b,inner_b);
%perim_outer = ellipsePerimeter(ratio_div_AB,1);
%vessel_wall_thickness_via_area = sqrt(area_outer/pi)-sqrt(area_inner/pi);
%vessel_wall_thickness_via_peri = perim_outer/(2*pi)-perim_inner/(2*pi);
end

function area = area_ellipse(a,b)

area = pi*a*b;

end

function b_approximation = b_approx(perimeter,rat)
t = ((rat-1)/(rat+1))^2;
b_approximation = perimeter/(pi*(rat+1)*(1 + 3*t/(10 + sqrt(4 - 3*t))));
%ramanujan approximation
end










