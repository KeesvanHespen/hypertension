img = dicomread('C:\Users\khespen\Documents\5. Hypertension\1. Working data\11012018_S025_S026\S026\ROI_0062-S026.dcm');

info = dicominfo('C:\Users\khespen\Documents\5. Hypertension\2. Original data\11012018_S025_S026\DICOM\IM_0062');
slope=getSubFieldval(info,'RescaleSlope');
inter=getSubFieldval(info,'RescaleIntercept');
img2=(img-inter)/slope;
Position = [-43.3908;19.7331;21.7782]; % get these from Mevislab
T1 = double(Position)+[0.108447;0.108447;-0.11]/2;
info.PerFrameFunctionalGroupsSequence.Item_1.PlanePositionSequence.Item_1.ImagePositionPatient=T1;
dicomwrite(img,'C:\Users\khespen\Documents\5. Hypertension\1. Working data\11012018_S025_S026\S026\ROI_0062-S0262.dcm',info,'CreateMode','copy')
