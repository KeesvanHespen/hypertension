% get all measured contour values
% Older method to combine all thickness and area measurements
%
% KM van Hespen, UMC Utrecht, 2020

cd('D:\SurfDrive\1. Original Data hypertension\Backupresults-03052018\matlab')
allcontourfiles = dir('*results_Needle*');
ht = ['S017';'S023';'S026';'S027';'S028';'S031';'S032';'S033';'S034';'S035';'S036';'S037'];

results=[];
for fl=1:numel(allcontourfiles)
    curcontourfile = load(allcontourfiles(fl).name);

    if any(sum(ht==allcontourfiles(fl).name(26:29),2)==4)
        switchht = 1;
    else 
        switchht = 0;
    end

        htorct = mat2cell(switchht*ones(1,size(curcontourfile.results.needle_vals,2)),1,ones(1,size(curcontourfile.results.needle_vals,2)));
        if size(curcontourfile.results.needle_vals,1)==7
            holdres = {curcontourfile.results.needle_vals;htorct;htorct};
        elseif size(curcontourfile.results.needle_vals,1)==6
            holdres = {curcontourfile.results.needle_vals;htorct;htorct;htorct};    
        else
            holdres = {curcontourfile.results.needle_vals;htorct};    
        end
        results{fl,1} = vertcat(holdres{:});
 end
results=horzcat(results{:});


% get thicknesses
structurenames = string(results(7,1:end));
meanthick = cell2mat(results(3,1:end));
medianthick = cell2mat(results(5,1:end));
ishtorct = cell2mat(results(end,1:end)');

structurenamesht = structurenames(ishtorct==1);
meanthickht = meanthick(ishtorct==1);
meanareaht = medianthick(ishtorct==1);

structurenamesct = structurenames(ishtorct==0);
meanthickct = meanthick(ishtorct==0);
meanareact = medianthick(ishtorct==0);
%%
% VA=A1,A2
% BA=A3,A6,B1
% AICA = A4,A5
% ICA = C5,C6
% MCA = C2,C4
% ACA = D1,D2, C1,C3
% PCoA = B4,B5
% PCA = B2,B3
% SCA = D3,D4

%% sort thicknesses hypertension based on structures
fillcellht={[];[];[];[];[];[];[];[];[]};
for istruct=1:numel(structurenamesht)
    curstruct = structurenamesht(istruct);
    if any(curstruct=='A1' | curstruct=='A2')
        col = 1;
    elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
        col = 2;  
    elseif any(curstruct=='A4' | curstruct=='A5')
        col = 9;
    elseif any(curstruct=='C5' | curstruct=='C6')
        col = 3;
    elseif any(curstruct=='C2' | curstruct=='C4')
        col = 4;
    elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
        col = 5;
    elseif any(curstruct=='B4' | curstruct=='B5')
        col = 6;
    elseif any( curstruct=='B2'| curstruct=='B3')
        col = 7;
    elseif any(curstruct=='D3' | curstruct=='D4')
        col = 8;
    end
    idx=sum(~cellfun(@isempty,{fillcellht{col,1,:}}),2);
    fillcellht{col,1,idx+1}=meanareaht(istruct);
    fillcellht{col,2,idx+1}=meanthickht(istruct);
end
%% sort thicknesses controls based on structures  
fillcellct={[];[];[];[];[];[];[];[];[]};
for istruct=1:numel(structurenamesct)
    curstruct = structurenamesct(istruct);
    if any(curstruct=='A1' | curstruct=='A2')
        col = 1;
    elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
        col = 2;  
    elseif any(curstruct=='A4' | curstruct=='A5')
        col = 9;
    elseif any(curstruct=='C5' | curstruct=='C6')
        col = 3;
    elseif any(curstruct=='C2' | curstruct=='C4')
        col = 4;
    elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
        col = 5;
    elseif any(curstruct=='B4' | curstruct=='B5')
        col = 6;
    elseif any(curstruct=='B2'| curstruct=='B3')
        col = 7;
    elseif any(curstruct=='D3' | curstruct=='D4')
        col = 8;
    end
    idx=sum(~cellfun(@isempty,{fillcellct{col,1,:}}),2);
    fillcellct{col,1,idx+1}=meanareact(istruct);
    fillcellct{col,2,idx+1}=meanthickct(istruct);
end
    
    
    
%%

matht = cellfun(@(x) mean(x(:,:,1)),fillcellht);
matct = cellfun(@(x) mean(x(:,:,1)),fillcellct);
matht(isnan(matht))=0;
matct(isnan(matct))=0;
stats=[];
for idx = 1:9
    for idy = 1:2
        resultsmatHT(idx,idy,1) = median(nonzeros(matht(idx,idy,:)));
        resultsmatHT(idx,idy,2) = iqr(nonzeros(matht(idx,idy,:)))/2;
        
        resultsmatCT(idx,idy,1) = median(nonzeros(matct(idx,idy,:)));
        resultsmatCT(idx,idy,2) = iqr(nonzeros(matct(idx,idy,:)))/2;
        stats(idx,idy)=ranksum(nonzeros(matht(idx,idy,:)),nonzeros(matct(idx,idy,:)));
        
    end
end
names = {'VA';'BA';'ICA';'MCA';'ACA';'PCoA';'PCA';'SCA';'AICA'};
figure;
pr=get(gca,'colororder');
hold on
bar(linspace(1,27,9), resultsmatHT(:,1,1),0.2,'FaceColor',pr(1,:))
bar(linspace(2,28,9), resultsmatCT(:,1,1),0.2,'FaceColor',pr(2,:))
errorbar(linspace(1,27,9), resultsmatHT(:,1,1), resultsmatHT(:,1,2),'k.')
errorbar(linspace(2,28,9), resultsmatCT(:,1,1), resultsmatCT(:,1,2),'k.')
title('median area (mm2)')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names)
legend('ht','ct')
hold off
for i=1:9
   a=linspace(1.5,27.5,9);
   text(a(i),0.8,num2str(stats(i,1)),'HorizontalAlignment','center') 
end
figure;
hold on
bar(linspace(1,27,9), resultsmatHT(:,2,1),0.2,'FaceColor',pr(1,:))
bar(linspace(2,28,9), resultsmatCT(:,2,1),0.2,'FaceColor',pr(2,:))

errorbar(linspace(1,27,9), resultsmatHT(:,2,1), resultsmatHT(:,2,2),'k.')
errorbar(linspace(2,28,9), resultsmatCT(:,2,1), resultsmatCT(:,2,2),'k.')
title('median thickness (mm)')
for i=1:9
   a=linspace(1.5,27.5,9);
   text(a(i),20,num2str(stats(i,2)),'HorizontalAlignment','center') 
end
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names)
legend('ht','ct')
hold off    


