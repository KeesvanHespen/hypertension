clear all;
load('results_ROI_0091-S021-1.mat');
mask_sav = 'results_Needle_ROI_0091-S021-1_more';

%% Make mask

if isfield(results,'thicknessmap_selection')
    thmap = results.thicknessmap_selection;
else
    thmap = results.thicknessmap;
end
thmap(thmap>2)=0;
img = results.Image/max(results.Image(:));
masksave = zeros(size(squeeze(img)));
roires={};
itt=1;
imagebrowse(mean(results.Image,3))

ct = cat(4,img,thmap);

while strcmp(questdlg('add needle?'),'Yes')
    if 1
        mask = itt*drawROI(permute(ct,[4 1 2 3]),-1,[],[],[-0.2 0.7]);
    
    prompt = {'Min slice range:','Max slice range:','ROI name:','dim 2:','dim 3:'};
    dlg_title = 'Input';
    num_lines = 1;

    mask_limit = inputdlg(prompt,dlg_title,num_lines);
    clckpoint(1)= str2num(mask_limit{4});
    clckpoint(2)= str2num(mask_limit{5});
    mask(:,:,1:(str2double(mask_limit{1})-1))=0;
    mask(:,:,(str2double(mask_limit{2})+1):end)=0;
    
    masksave = masksave+mask;
    else
        mask = (results2.needle_rois==itt);
        clickedpoint={1,1};
    end
    masked = logical(mask).*thmap;
    roires{1,itt} = itt;
    roires{2,itt} = clckpoint;
    roires{3,itt} = mean(nonzeros(masked));
    roires{4,itt} = std(nonzeros(masked));
    roires{5,itt} = median(nonzeros(masked));
    roires{6,itt} = iqr(nonzeros(masked));
    roires{7,itt} = mask_limit{3};
    roires{8,itt} = nonzeros(masked)
    itt=itt+1;
end


%% save
results.needle_rois = masksave;
results.needle_vals = roires;
save(mask_sav,'results','-v7.3');


