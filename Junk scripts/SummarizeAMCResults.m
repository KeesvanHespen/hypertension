amcstructnames = ['A1'	;'A2'	;'A3'	;'A4'	;'A5'	;'A6'	;'B1'	;'B2'	;'B3'	;'B4'	;'B5'	;'B6'	;'C1'	;'C2'	;'C3'	;'C4'	;'C5'	;'C6'	;'D1'	;'D2'	;'D3'	;'D4'];
subjnameorder=['S015';'S017';'S022';'S021';'S020';'S019';'S023';'S024';'S026';'S025';'S027';'S028';'S031';'S032';'S029';'S030';'S034';'S033';'S035';'S036'];
ht = ['S017';'S023';'S026';'S027';'S028';'S031';'S032';'S033';'S034';'S035';'S036';'S037'];
male = [1,1,0,1,1,1,0,1,0,0,0,0,1,1,0,0,0,1,0,1];
load('C:\Users\khespen\Documents\5. Hypertension\amcresultsarea.mat')
load('C:\Users\khespen\Documents\5. Hypertension\amcresultsthickness.mat')
load('C:\Users\khespen\Documents\5. Hypertension\amcresultsintimamediathickness.mat')

fillcellht={[];[];[];[];[];[];[];[];[]};
fillcellct={[];[];[];[];[];[];[];[];[]};

for idy = 1:size(amcstructnames,1)
     curstruct = string(amcstructnames(idy,:));
     if any(curstruct=='A1' | curstruct=='A2')
            col = 1;
        elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
            col = 2;  
        elseif any(curstruct=='A4' | curstruct=='A5')
            col = 9;
        elseif any(curstruct=='C5' | curstruct=='C6')
            col = 3;
        elseif any(curstruct=='C2' | curstruct=='C4')
            col = 4;
        elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
            col = 5;
        elseif any(curstruct=='B4' | curstruct=='B5')
            col = 6;
        elseif any( curstruct=='B2'| curstruct=='B3')
            col = 7;
        elseif any(curstruct=='D3' | curstruct=='D4')
            col = 8;
    end
    for ind = 1:size(subjnameorder,1)     
        if any(sum(ht==subjnameorder(ind,:),2)==4)
            fillcellht{col}= [fillcellht{col}, amcresultsarea(ind,idy)];
        else
            fillcellct{col}= [fillcellct{col}, amcresultsarea(ind,idy)];
        end
    end
end

stats=[];
quantile25ct=[];
midquantile=[];
midquantilect=[];
lengthbar=[];
lengthbarct=[];
quantile75ct=[];
normalize=1;
%%

for idx = 1:9
        normalize=1;
        %normalize=median(nonzeros(fillcellct{idx,:}));
        quantile25 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.25);
        quantile75 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.75);
        resultsmatHT(idx,1) = median(nonzeros(fillcellht{idx,:})/normalize);
        resultsmatHT(idx,2) = iqr(nonzeros(fillcellht{idx,:})/normalize)/2;
        
        resultsmatCT(idx,1) = median(nonzeros(fillcellct{idx,:})/normalize);
        resultsmatCT(idx,2) = iqr(nonzeros(fillcellct{idx,:})/normalize)/2;
        stats(idx)=ranksum(nonzeros(fillcellht{idx,:}),nonzeros(fillcellct{idx,:}));
        
        quantile25ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.25);
        quantileremember25(idx,1) = quantile25;
        quantileremember75(idx,1) = quantile75;
        quantile75ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.75);
        midquantile(idx,1) = [(quantile75+quantile25)/2];
        lengthbar(idx,1)= quantile75-quantile25;
        quantileremember25ct(idx,1) = quantile25ct;
        quantileremember75ct(idx,1) = quantile75ct;
        midquantilect(idx,1) = [(quantile75ct+quantile25ct)/2];
        lengthbarct(idx,1)= quantile75ct-quantile25ct;

end


  
names = {'VA';'BA';'ICA';'MCA';'ACA';'PCoA';'PCA';'SCA';'AICA'};
figure;
pr=get(gca,'colororder');
hold on; box on;
bar(linspace(1,27,9), resultsmatHT(:,1,1),0.2,'FaceColor',pr(1,:),'linewidth',2,'EdgeColor','w')
bar(linspace(2,28,9), resultsmatCT(:,1,1),0.2,'FaceColor',pr(2,:),'linewidth',2,'EdgeColor','w')
errorbar(linspace(1,27,9),midquantile(:,1)...
 ,lengthbar(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
errorbar(linspace(2,28,9),midquantilect(:,1)...
 ,lengthbarct(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)

ylabel('Vessel wall area (mm2)'); 
set(gcf,'color','k')
set(gca,'linewidth',2,'Color','k','Zcolor','w','Xcolor','w','Ycolor','w')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names,'FontSize',16,'linewidth',2)
for i=1:9
   a=linspace(1.5,27.5,9);
  % text(a(i),1+max(midquantile(:,1)+lengthbar(:,1)/2),num2str(stats(i,1),2),'HorizontalAlignment','center','FontWeight','bold') 
end
leg=legend('ht','ct','Location','northeastoutside')
set(leg,'textColor','w','EdgeColor','w','FontSize',16)
ylim([0 9])
hold off




%%%%
%%

fillcellht={[];[];[];[];[];[];[];[];[]};
fillcellct={[];[];[];[];[];[];[];[];[]};

for idy = 1:size(amcstructnames,1)
     curstruct = string(amcstructnames(idy,:));
     if any(curstruct=='A1' | curstruct=='A2')
            col = 1;
        elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
            col = 2;  
        elseif any(curstruct=='A4' | curstruct=='A5')
            col = 9;
        elseif any(curstruct=='C5' | curstruct=='C6')
            col = 3;
        elseif any(curstruct=='C2' | curstruct=='C4')
            col = 4;
        elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
            col = 5;
        elseif any(curstruct=='B4' | curstruct=='B5')
            col = 6;
        elseif any( curstruct=='B2'| curstruct=='B3')
            col = 7;
        elseif any(curstruct=='D3' | curstruct=='D4')
            col = 8;
    end
    for ind = 1:size(subjnameorder,1)     
        if any(sum(ht==subjnameorder(ind,:),2)==4)
            fillcellht{col}= [fillcellht{col}, amcresultsthickness(ind,idy)/1000];
        else
            fillcellct{col}= [fillcellct{col}, amcresultsthickness(ind,idy)/1000];
        end
    end
end

stats=[];
quantile25ct=[];
midquantile=[];
midquantilect=[];
lengthbar=[];
lengthbarct=[];
quantile75ct=[];
normalize=1;
%%

for idx = 1:9
        normalize=1;
        %normalize=median(nonzeros(fillcellct{idx,:}));
        quantile25 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.25);
        quantile75 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.75);
        resultsmatHT(idx,1) = median(nonzeros(fillcellht{idx,:})/normalize);
        resultsmatHT(idx,2) = iqr(nonzeros(fillcellht{idx,:})/normalize)/2;
        
        resultsmatCT(idx,1) = median(nonzeros(fillcellct{idx,:})/normalize);
        resultsmatCT(idx,2) = iqr(nonzeros(fillcellct{idx,:})/normalize)/2;
        stats(idx)=ranksum(nonzeros(fillcellht{idx,:}),nonzeros(fillcellct{idx,:}));
        
        quantile25ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.25);
        quantileremember25(idx,1) = quantile25;
        quantileremember75(idx,1) = quantile75;
        quantile75ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.75);
        midquantile(idx,1) = [(quantile75+quantile25)/2];
        lengthbar(idx,1)= quantile75-quantile25;
        quantileremember25ct(idx,1) = quantile25ct;
        quantileremember75ct(idx,1) = quantile75ct;
        midquantilect(idx,1) = [(quantile75ct+quantile25ct)/2];
        lengthbarct(idx,1)= quantile75ct-quantile25ct;

end


  
names = {'VA';'BA';'ICA';'MCA';'ACA';'PCoA';'PCA';'SCA';'AICA'};
figure;
pr=get(gca,'colororder');
hold on; box on;
bar(linspace(1,27,9), resultsmatHT(:,1,1),0.2,'FaceColor',pr(1,:),'linewidth',2,'EdgeColor','w')
bar(linspace(2,28,9), resultsmatCT(:,1,1),0.2,'FaceColor',pr(2,:),'linewidth',2,'EdgeColor','w')
errorbar(linspace(1,27,9),midquantile(:,1)...
 ,lengthbar(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
errorbar(linspace(2,28,9),midquantilect(:,1)...
 ,lengthbarct(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)

ylabel('Vessel wall thickness (mm)'); 
set(gcf,'color','k')
set(gca,'linewidth',2,'Color','k','Zcolor','w','Xcolor','w','Ycolor','w')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names,'FontSize',16,'linewidth',2)
for i=1:9
   a=linspace(1.5,27.5,9);
  % text(a(i),1+max(midquantile(:,1)+lengthbar(:,1)/2),num2str(stats(i,1),2),'HorizontalAlignment','center','FontWeight','bold') 
end
leg=legend('ht','ct','Location','northeastoutside')
set(leg,'textColor','w','EdgeColor','w','FontSize',16)
ylim([0 .9])
hold off


%%%%
%%

fillcellht={[];[];[];[];[];[];[];[];[]};
fillcellct={[];[];[];[];[];[];[];[];[]};

for idy = 1:size(amcstructnames,1)
     curstruct = string(amcstructnames(idy,:));
     if any(curstruct=='A1' | curstruct=='A2')
            col = 1;
        elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
            col = 2;  
        elseif any(curstruct=='A4' | curstruct=='A5')
            col = 9;
        elseif any(curstruct=='C5' | curstruct=='C6')
            col = 3;
        elseif any(curstruct=='C2' | curstruct=='C4')
            col = 4;
        elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
            col = 5;
        elseif any(curstruct=='B4' | curstruct=='B5')
            col = 6;
        elseif any( curstruct=='B2'| curstruct=='B3')
            col = 7;
        elseif any(curstruct=='D3' | curstruct=='D4')
            col = 8;
    end
    for ind = 1:size(subjnameorder,1)     
        if any(sum(ht==subjnameorder(ind,:),2)==4)
            fillcellht{col}= [fillcellht{col}, amcresultsintimamediathickness(ind,idy)/1000];
        else
            fillcellct{col}= [fillcellct{col}, amcresultsintimamediathickness(ind,idy)/1000];
        end
    end
end

stats=[];
quantile25ct=[];
midquantile=[];
midquantilect=[];
lengthbar=[];
lengthbarct=[];
quantile75ct=[];
normalize=1;
%%

for idx = 1:9
        normalize=1;
        %normalize=median(nonzeros(fillcellct{idx,:}));
        quantile25 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.25);
        quantile75 = quantile(nonzeros(fillcellht{idx,:})/normalize,0.75);
        resultsmatHT(idx,1) = median(nonzeros(fillcellht{idx,:})/normalize);
        resultsmatHT(idx,2) = iqr(nonzeros(fillcellht{idx,:})/normalize)/2;
        
        resultsmatCT(idx,1) = median(nonzeros(fillcellct{idx,:})/normalize);
        resultsmatCT(idx,2) = iqr(nonzeros(fillcellct{idx,:})/normalize)/2;
        stats(idx)=ranksum(nonzeros(fillcellht{idx,:}),nonzeros(fillcellct{idx,:}));
        
        quantile25ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.25);
        quantileremember25(idx,1) = quantile25;
        quantileremember75(idx,1) = quantile75;
        quantile75ct = quantile(nonzeros(fillcellct{idx,:})/normalize,0.75);
        midquantile(idx,1) = [(quantile75+quantile25)/2];
        lengthbar(idx,1)= quantile75-quantile25;
        quantileremember25ct(idx,1) = quantile25ct;
        quantileremember75ct(idx,1) = quantile75ct;
        midquantilect(idx,1) = [(quantile75ct+quantile25ct)/2];
        lengthbarct(idx,1)= quantile75ct-quantile25ct;

end


  
names = {'VA';'BA';'ICA';'MCA';'ACA';'PCoA';'PCA';'SCA';'AICA'};
figure;
pr=get(gca,'colororder');
hold on; box on;
bar(linspace(1,27,9), resultsmatHT(:,1,1),0.2,'FaceColor',pr(1,:),'linewidth',2,'EdgeColor','w')
bar(linspace(2,28,9), resultsmatCT(:,1,1),0.2,'FaceColor',pr(2,:),'linewidth',2,'EdgeColor','w')
errorbar(linspace(1,27,9),midquantile(:,1)...
 ,lengthbar(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
errorbar(linspace(2,28,9),midquantilect(:,1)...
 ,lengthbarct(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)

ylabel('Vessel wall thickness (mm)'); 
set(gcf,'color','k')
set(gca,'linewidth',2,'Color','k','Zcolor','w','Xcolor','w','Ycolor','w')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names,'FontSize',16,'linewidth',2)
for i=1:9
   a=linspace(1.5,27.5,9);
  % text(a(i),1+max(midquantile(:,1)+lengthbar(:,1)/2),num2str(stats(i,1),2),'HorizontalAlignment','center','FontWeight','bold') 
end
leg=legend('ht','ct','Location','northeastoutside')
set(leg,'textColor','w','EdgeColor','w','FontSize',16)
ylim([0 .9])
hold off
