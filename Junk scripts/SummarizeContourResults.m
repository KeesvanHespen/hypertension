% get all measured contour values
% Old file used to compute the vessel wall thickness and area from
% Contour_results*.mat files
%
% KM van Hespen, UMC Utrecht, 2020

cd('D:\SurfDrive\1. Original Data hypertension\Backupresults-03052018\matlab3_reworked_contours')
allcontourfiles = dir('Contour_results*.mat');
ht = ['S017';'S023';'S026';'S027';'S028';'S031';'S032';'S033';'S034';'S035';'S036';'S037';'S039';'S041';'S043';'S048';'S050';'S052';'S053';'S054';'S055';'S056';'S062';'S063';'S064';'S065'];
ht=['S015';'S017';'S019';'S020';'S021';'S022';'S023';'S024';'S025';'S026';'S027';'S028';'S029';'S030';'S031';'S032';'S033';'S034';'S035';'S036';'S037';'S038';'S039';'S040';'S041';'S042';'S043';'S044';'S045';'S046';'S047';'S048';'S049';'S050';'S051';'S052';'S053';'S054';'S055';'S056';'S057';'S058';'S059';'S060';'S061';'S062';'S063';'S064';'S065';'S066';'S067';'S068'];
ag={'S015','S017','S019','S020','S021','S022','S023','S024','S025','S026','S027','S028','S029','S030','S031','S032','S033','S034','S035','S036','S037','S038','S039','S040','S041','S042','S043','S044','S045','S046','S047','S048','S049','S050','S051','S052','S053','S054','S055','S056','S057','S058','S059','S060','S061','S062','S063','S064','S065','S066','S067','S068';
    56,78,70,62,86,62,72,68,58,64,64,80,82,74,74,71,74,87,69,68,64,53,72,66,55,72,61,56,65,70,80,65,87,54,71,67,60,87,84,68,58,63,67,71,54,62,63,69,64,64,65,77;
    1,1,1,1,1,0,0,1,0,0,0,0,0,0,1,1,1,0,0,1,0,0,1,0,1,1,1,1,0,1,0,0,0,1,1,13,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1};

results=[];
for fl=1:numel(allcontourfiles)
    curcontourfile = load(allcontourfiles(fl).name);
    subjname={};
    if find(sum(ht==allcontourfiles(fl).name(26:29),2)==4)
        switchht = ag{3,find(sum(ht==allcontourfiles(fl).name(26:29),2)==4)};
    else 
        switchht = ag{3,find(sum(ht==allcontourfiles(fl).name(26:29),2)==4)};
    end
    age = ag(2,find(strcmp(ag(1,:),allcontourfiles(fl).name(26:29))==1));
    age=age{1};
    mf = ag(3,find(strcmp(ag(1,:),allcontourfiles(fl).name(26:29))==1));
    mf=mf{1};
    if fl>1
        htorct = mat2cell(switchht*ones(1,size(curcontourfile.results,2)-1),1,ones(1,size(curcontourfile.results,2)-1));
        ageline = mat2cell(age*ones(1,size(curcontourfile.results,2)-1),1,ones(1,size(curcontourfile.results,2)-1));
        mfline = mat2cell(mf*ones(1,size(curcontourfile.results,2)-1),1,ones(1,size(curcontourfile.results,2)-1));

        [subjname{1:numel(htorct)}]=deal(allcontourfiles(fl).name(26:29));
        results{fl,1} = {curcontourfile.results(:,2:end);htorct;subjname;ageline;mfline};
        results{fl,1} = vertcat(results{fl,1}{:});
        
    else
        
        htorct = mat2cell([0,switchht*ones(1,size(curcontourfile.results,2)-1)],1,ones(1,size(curcontourfile.results,2)));
        ageline = mat2cell([0,age*ones(1,size(curcontourfile.results,2)-1)],1,ones(1,size(curcontourfile.results,2)));
        mfline = mat2cell([0,mf*ones(1,size(curcontourfile.results,2)-1)],1,ones(1,size(curcontourfile.results,2)));
       
        [subjname{1:numel(htorct)}]=deal(allcontourfiles(fl).name(26:29));
        results{fl,1} = {curcontourfile.results;htorct;subjname;ageline;mfline};
        results{fl,1}= vertcat(results{fl,1}{:});
    end
end

results=horzcat(results{:});

quantile25=[];
quantile75=[];
maleorfemale = results(end,:)';
thresholds = results(9,:)';
haus= results(12,:)';
selection= [1;cell2mat(thresholds(2:end))<0.551 & cell2mat(thresholds(2:end))>0.449 & cell2mat(maleorfemale(2:end))>-1];
cleanedcontourresults=[];
for ind = 1:numel(thresholds)
    if selection(ind)
        cleanedcontourresults{end+1}= {results{:,ind}}';
    end
end
cleanedcontourresults = horzcat(cleanedcontourresults{:});
%%
% get thicknesses
structurenames = string(cleanedcontourresults(1,2:end));
meanthick = cell2mat(cleanedcontourresults(8,2:end));
meanarea = cell2mat(cleanedcontourresults(6,2:end));
ishtorct = cell2mat(cleanedcontourresults(end-3,2:end));

structurenamesht = structurenames(ishtorct==1);
meanthickht = meanthick(ishtorct==1);
meanareaht = meanarea(ishtorct==1);

structurenamesct = structurenames(ishtorct==0);
meanthickct = meanthick(ishtorct==0);
meanareact = meanarea(ishtorct==0);
%%
% VA=A1,A2
% BA=A3,A6,B1
% AICA = A4,A5
% ICA = C5,C6
% MCA = C2,C4
% ACA = D1,D2, C1,C3
% PCoA = B4,B5
% PCA = B2,B3
% SCA = D3,D4

%% sort thicknesses hypertension based on structures
fillcellht={[];[];[];[];[];[];[];[];[]};
for istruct=1:numel(structurenamesht)
    curstruct = structurenamesht(istruct);
    if any(curstruct=='A1' | curstruct=='A2')
        col = 1;
    elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
        col = 2;  
    elseif any(curstruct=='A4' | curstruct=='A5')
        col = 9;
    elseif any(curstruct=='C5' | curstruct=='C6')
        col = 3;
    elseif any(curstruct=='C2' | curstruct=='C4')
        col = 4;
    elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
        col = 5;
    elseif any(curstruct=='B4' | curstruct=='B5')
        col = 6;
    elseif any( curstruct=='B2'| curstruct=='B3')
        col = 7;
    elseif any(curstruct=='D3' | curstruct=='D4')
        col = 8;
    end
    idx=sum(~cellfun(@isempty,{fillcellht{col,1,:}}),2);
    fillcellht{col,1,idx+1}=meanareaht(istruct);
    fillcellht{col,2,idx+1}=meanthickht(istruct);
end
%% sort thicknesses controls based on structures  
fillcellct={[];[];[];[];[];[];[];[];[]};
for istruct=1:numel(structurenamesct)
    curstruct = structurenamesct(istruct);
    if any(curstruct=='A1' | curstruct=='A2')
        col = 1;
    elseif any(curstruct=='B1' |curstruct=='A3' | curstruct=='A6')
        col = 2;  
    elseif any(curstruct=='A4' | curstruct=='A5')
        col = 9;
    elseif any(curstruct=='C5' | curstruct=='C6')
        col = 3;
    elseif any(curstruct=='C2' | curstruct=='C4')
        col = 4;
    elseif any(curstruct=='D1' | curstruct=='D2' | curstruct=='C1' | curstruct=='C3')
        col = 5;
    elseif any(curstruct=='B4' | curstruct=='B5')
        col = 6;
    elseif any(curstruct=='B2'| curstruct=='B3')
        col = 7;
    elseif any(curstruct=='D3' | curstruct=='D4')
        col = 8;
    end
    idx=sum(~cellfun(@isempty,{fillcellct{col,1,:}}),2);
    fillcellct{col,1,idx+1}=meanareact(istruct);
    fillcellct{col,2,idx+1}=meanthickct(istruct);
end
    
    
    
%%

matht = cellfun(@(x) mean(x(:,:,1)),fillcellht);
matct = cellfun(@(x) mean(x(:,:,1)),fillcellct);
matht(isnan(matht))=0;
matct(isnan(matct))=0;
stats=[];
quantile25ct=[];
midquantile=[];
midquantilect=[];
lengthbar=[];
lengthbarct=[];
quantile75ct=[];
normalize=1;
permatct = permute(matct,[3,1,2]);
permatht = permute(matht,[3,1,2]);

for idx = 1:9
    for idy = 1:2
        %normalize=median(nonzeros(matct(idx,idy,:)));
        quantile25 = quantile(nonzeros(matht(idx,idy,:))/normalize,0.25);
        quantile75 = quantile(nonzeros(matht(idx,idy,:))/normalize,0.75);
        resultsmatHT(idx,idy,1) = median(nonzeros(matht(idx,idy,:))/normalize);
        resultsmatHT(idx,idy,2) = iqr(nonzeros(matht(idx,idy,:))/normalize)/2;
        
        resultsmatCT(idx,idy,1) = median(nonzeros(matct(idx,idy,:))/normalize);
        resultsmatCT(idx,idy,2) = iqr(nonzeros(matct(idx,idy,:))/normalize)/2;
        stats(idx,idy)=ranksum(nonzeros(matht(idx,idy,:)),nonzeros(matct(idx,idy,:)));
        quantile25ct = quantile(nonzeros(matct(idx,idy,:))/normalize,0.25);
        quantileremember25(idx,idy,1) = quantile25;
        quantileremember75(idx,idy,1) = quantile75;
        quantile75ct = quantile(nonzeros(matct(idx,idy,:))/normalize,0.75);
        midquantile(idx,idy,1) = [(quantile75+quantile25)/2];
        lengthbar(idx,idy,1)= quantile75-quantile25;
        quantileremember25ct(idx,idy,1) = quantile25ct;
        quantileremember75ct(idx,idy,1) = quantile75ct;
        midquantilect(idx,idy,1) = [(quantile75ct+quantile25ct)/2];
        lengthbarct(idx,idy,1)= quantile75ct-quantile25ct;
    end
end


  
names = {'VA';'BA';'ICA';'MCA';'ACA';'PCoA';'PCA';'SCA';'AICA'};
figure;
pr=get(gca,'colororder');
hold on; box on;
bar(linspace(1,27,9), resultsmatHT(:,1,1),0.2,'FaceColor',pr(1,:),'linewidth',2,'EdgeColor','w')
bar(linspace(2,28,9), resultsmatCT(:,1,1),0.2,'FaceColor',pr(2,:),'linewidth',2,'EdgeColor','w')
errorbar(linspace(1,27,9),midquantile(:,1)...
 ,lengthbar(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
errorbar(linspace(2,28,9),midquantilect(:,1)...
 ,lengthbarct(:,1)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)

ylabel('Vessel wall area (mm2)'); 
set(gcf,'color','k')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names,'FontSize',16,'linewidth',2,'Color','w','Zcolor','w','Xcolor','w','Ycolor','w')
for i=1:9
   a=linspace(1.5,27.5,9);
   text(a(i),1+max(midquantile(:,1)+lengthbar(:,1)/2),num2str(stats(i,1),2),'HorizontalAlignment','center','FontWeight','bold') 
end
leg = legend('ht','ct','Location','northeastoutside');
set(leg,'TextColor','k','EdgeColor','w','FontSize',16)
ylim([0 1.45+max(midquantile(:,1)+lengthbar(:,1)/2)])
hold off

figure;
hold on; box on
bar(linspace(1,27,9), resultsmatHT(:,2,1),0.2,'FaceColor',pr(1,:),'linewidth',2,'EdgeColor','w')
bar(linspace(2,28,9), resultsmatCT(:,2,1),0.2,'FaceColor',pr(2,:),'linewidth',2,'EdgeColor','w')

errorbar(linspace(1,27,9),midquantile(:,2)...
 ,lengthbar(:,2)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
errorbar(linspace(2,28,9),midquantilect(:,2)...
 ,lengthbarct(:,2)/2,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
ylabel('Vessel wall thickness (mm)'); 
set(gcf,'color','k')
set(gca,'xtick',linspace(1.5,27.5,9),'xticklabel',names,'FontSize',16,'linewidth',2,'Color','w','Zcolor','w','Xcolor','w','Ycolor','w')
for i=1:9
   a=linspace(1.5,27.5,9);
   text(a(i),0.1+max(midquantile(:,2)+lengthbar(:,2)/2),num2str(stats(i,2),2),'HorizontalAlignment','center','FontWeight','bold') 
end
ylim([0 0.15+max(midquantile(:,2)+lengthbar(:,2)/2)])
leg=legend('ht','ct','Location','northeastoutside');
set(leg,'TextColor','k','EdgeColor','w','FontSize',16)
hold off    


%%
%age = cell2mat({cleanedcontourresults{15,:}});
%thick = cell2mat({cleanedcontourresults{8,2:end}});

%indices = find(strcmp(cleanedcontourresults(1,:),'C2')==1 | strcmp(cleanedcontourresults(1,:),'C4')==1)
%scatter(age(indices),thick(indices));