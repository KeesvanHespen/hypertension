#!/usr/bin/env python

"""
Python script employing Lasagne and Theano to calculate the vessel wall 
thickness from image patches at any given resolution."""
from mevis import *
import numpy as np
import h5py
import scipy.io as sio

def draw():
  global resultshr, var
  f = h5py.File(ctx.field("matfilename").value,'r')
  resultshr = f['results']
  f2 = sio.loadmat(ctx.field("maskmatfilename").value)
  mask = f2['mask']
  try:
    thmap = np.transpose(np.transpose(np.array(resultshr['thicknessmapselection']), (2,1, 0))*np.array(mask) , (2,0,1))
    var=1
  except:
    thmap = np.transpose(np.transpose(np.array(resultshr['thicknessmap_selection']), (2, 1, 0))*np.array(mask) , (2,0, 1))
    var=0
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(thmap, minMaxValues =(np.min(thmap),np.max(thmap)))
  
def save():
  ctx.field("itkImageFileWriter.save").touch()
   
def setoverlay():
  if ctx.field("booleanswitch").boolValue()==False:
    ctx.field("Switch.currentInput").setValue(0)
  else:
    ctx.field("Switch.currentInput").setValue(1)
      