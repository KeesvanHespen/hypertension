Code used for the Hypertension paper.

Measure_VWT_v3.1 was used to measure the vessel wall thickness and area of circle of Willis specimens. This implementation was written for MeVisLab 3.1.

Mask_mat_files was used to (de)select certain parts of the vessel wall, that for example were dissected or contained air.

Summarize_contour_contour_results.m was used to aggregate all measurements from MRI and histology and visualize them.

Junk scripts are not directly used for the manuscripts.


KM van Hespen, UMC Utrecht, 2020