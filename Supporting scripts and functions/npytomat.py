# -*- coding: utf-8 -*-
"""
Created on Thu May  3 16:08:50 2018

Script to convert numpy (.npy) files into matlab (.mat) files

@author: KM van Hespen, UMC Utrecht, 2018
"""

import scipy.io as sio
import numpy as np
import os

filelist=[]
for root, dirs, files in os.walk("D:/5. NN/3. Data/"):
    for file in files:
        if file.startswith("samples_0.4000370037_acq_0_test_2019-04-02_11;48;55_augmentedLR") and file.endswith(".npy"):
             filelist.append(os.path.join(root, file))

for contourfile in filelist:
    contourresults = np.load(contourfile,encoding='latin1')
    
    sio.savemat(contourfile[:-4]+'.mat',{'results': contourresults})