from mevis import *
import numpy as np
import os
import glob
import sys
import matplotlib.pyplot as plt
import datetime
import pydicom
import scipy.io as sio
import subprocess as subp
from pyflann import *

def init():
    ctx.field("maxi").setValue(100)
    ctx.field("setstruct").setValue(0)
  
def computehaus():
    # computes the hausdorff distance
    global allhaus, allinnercontours, resultscont, allareas, allthickness
    text = np.array(['Structure name', 'median_inner_to_outer_distance', 'median_outer_to_inner_distance', 'iqr_inner_to_outer_distance', 'iqr_outer_to_inner_distance', 'mean_inner_to_outer_distance', 'mean_outer_to_inner_distance', 'std_inner_to_outer_distance', 'std_outer_to_inner_distance', 'mean_length_inner', 'mean_length_outer', 'mean_area_inner', 'mean_area_outer', 'threshold_contour'])
    text = text[:, None]
    allhaus = []
    allareas = []
    allthickness = []
    path, filename = os.path.split(ctx.field("contourresultname").stringValue())
    if os.path.split(path)[1]  == "Contours":
        os.chdir(os.path.split(path)[0])
        path_above_contours = os.path.split(path)[0]
    else:
        path_above_contours = path

    # load the results npy
    resultscont = np.load(ctx.field("contourresultname").value, encoding='latin1')

    if not os.path.split(path)[1] == "Contours":
      os.chdir(os.path.join(path,'Contours'))
    else:
      os.chdir(path)
    # Find all inner and outer contour files in the current directory
    allinnercontours = glob.glob('Inner_*' + filename[24:29] + '*')
    alloutercontours = glob.glob('Outer_*' + filename[24:29] + '*')
    ctx.field("maxi").setValue(len(allinnercontours)-1)

    mean_values_haus = []
    allcontournames = []
    for curinner, curouter in zip(allinnercontours, alloutercontours):
      curareas = resultscont[9, np.where((resultscont[0,:]  == curinner[6:8]) * np.append(False, np.abs(resultscont[8, 1:] - 0.5) < 0.8))[0][0]]
      curthickness = resultscont[10, np.where((resultscont[0,:]  == curinner[6:8]) * np.append(False, np.abs(resultscont[8, 1:] - 0.5) < 0.8))[0][0]]
      ctx.field("CSOLoad.fileName").setStringValue(curinner)
      ctx.field("CSOLoad1.fileName").setStringValue(curouter)
      hausddistance = np.zeros((7, int(ctx.field("CSOInfo.numCSOs").value)), dtype='object')

      for curcontindex in range(1,int(ctx.field("CSOInfo.numCSOs").value) + 1):
        ctx.field("CSOFilter2.inputCSOString").setStringValue(str(curcontindex))
        ctx.field("CSOFilter1.inputCSOString").setStringValue(str(curcontindex))
        # get inner and outer contour path point locations for the current contour index
        outerlist = ctx.field("CSOFilter1.outputCSOList").object()
        outer = outerlist.getCSOById(curcontindex)
        outercont = np.vstack((outer.getPathPointsAsNumPyArray(),outer.getSeedPointsAsNumPyArray()))
        innerlist = ctx.field("CSOFilter2.outputCSOList").object()
        inner = innerlist.getCSOById(curcontindex)
        innercont = np.vstack((inner.getPathPointsAsNumPyArray(),inner.getSeedPointsAsNumPyArray()))

        if ctx.field("CSOInfo1.csoPathPointColor").value  == (0, 1, 0) and (np.abs(ctx.field("CSOInfo2.csoLength").value - ctx.field("CSOInfo1.csoLength").value) > 1e-2 or np.abs(ctx.field("CSOInfo2.csoArea").value - ctx.field("CSOInfo1.csoArea").value) > 1e-2):
          hausddistance[0, curcontindex - 1] = get_distances(innercont,outercont)
          hausddistance[1, curcontindex - 1] = get_distances(outercont,innercont)
          hausddistance[2, curcontindex - 1] = ctx.field("CSOInfo1.csoLength").floatValue()
          hausddistance[3, curcontindex - 1] = ctx.field("CSOInfo2.csoLength").floatValue()
          hausddistance[4, curcontindex - 1] = ctx.field("CSOInfo1.csoArea").floatValue()
          hausddistance[5, curcontindex - 1] = ctx.field("CSOInfo2.csoArea").floatValue()
          hausddistance[6, curcontindex - 1] = resultscont[8, np.where((resultscont[0,:] == curinner[6:8]) * np.append(False, np.abs(resultscont[8, 1:] - 0.5) < 0.26))[0][0]]
        else:
          hausddistance[0, curcontindex - 1] = np.array([])
          hausddistance[1, curcontindex - 1] = np.array([])
          hausddistance[2, curcontindex - 1] = 0
          hausddistance[3, curcontindex - 1] = 0
          hausddistance[4, curcontindex - 1] = 0
          hausddistance[5, curcontindex - 1] = 0
          hausddistance[6, curcontindex - 1] = 0
      allcontournames.append(curinner[6:8])
      # Save all the computed hausdorff distances
      np.save(os.path.join(path,'Contours','Hausdorff_' +  curinner[:-4]),hausddistance)
      
      # Compute the mean hausdorff distance per anatomical location
      allareas.append(curareas)
      allthickness.append(curthickness)
      allhaus.append(hausddistance)
      inner_outer = np.hstack(hausddistance[1, :])
      outer_inner = np.hstack(hausddistance[0, :])
      mean_values_haus.append(np.hstack([np.median(inner_outer, axis=0), np.median(outer_inner), np.percentile(inner_outer, 75) - np.percentile(inner_outer, 25), np.percentile(outer_inner, 75) - np.percentile(outer_inner,25), np.mean(inner_outer), np.mean(outer_inner), np.std(inner_outer), np.std(outer_inner), np.true_divide(hausddistance[2:].sum(1), (hausddistance[2:] != 0).sum(1))]))
    results = np.transpose(np.concatenate([np.transpose(text),np.concatenate([np.array(allcontournames,dtype = object)[:,None],np.vstack(mean_values_haus)],axis = 1)],axis = 0))
    # Save the averaged computed hausdorff distance
    np.save(os.path.join(path_above_contours,'Contour-contour_distances_new_' + curinner[9:-4]), results)
    sio.savemat(os.path.join(path_above_contours,'Contour-contour_distances_new_' + curinner[9:-4]) + '.mat', {'results':results})
    ctx.field("setstruct").setValue(0)
    save()
    ctx.field("savetext").setStringValue("Saved!")
    print('finished!')

def get_distances(xmarklist1, xmarklist2):
    # Uses flann to compute the hausdorff distance between two xmarkerlists.
    flann = FLANN()
    result, dists = flann.nn(xmarklist1, xmarklist2, 1, algorithm="kmeans", branching=32, iterations=7, checks=16)
    coord_diff = xmarklist2 - xmarklist1[result,:]
    all_distances = np.sqrt(np.sum(np.power(coord_diff, 2), axis=1))
    return np.array(all_distances)

def save():
    ctx.field("Counter1.autoStep").setBoolValue(True)

