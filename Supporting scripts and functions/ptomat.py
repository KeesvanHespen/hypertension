# -*- coding: utf-8 -*-
"""
Created on Thu May  3 16:08:50 2018

Script to convert pickled files (.p) into matlab (.mat) files

@author: KM van Hespen, UMC Utrecht, 2018
"""

import scipy.io as sio
import numpy as np
import os
import pickle

filelist=[]
for root, dirs, files in os.walk("D:/5. NN/3. Data/"):
    for file in files:
        if file.startswith("samples_0.4000370037_acq_98_test_2019-04-25_09;31;45_augmentedLR") and file.endswith(".p"):
             filelist.append(os.path.join(root, file))

for contourfile in filelist:
    contourresults = pickle.load( open( contourfile, "rb" ) )
    
    sio.savemat(contourfile[:-4] + '.mat', {'results': contourresults})