function output = darktheme(varargin)
box on;
if nargin
    set(0, 'CurrentFigure', varargin(1))
end
pr=get(gca,'colororder');
%find bar plots
bars = findobj(gcf,'Type','bar');
for bar=1:numel(bars)
    set(bars(bar),'FaceColor',pr(bar-floor(bar/8)*7,:),'linewidth',2,'EdgeColor','w');
end
%find legends
legs=findobj(gcf,'Type','legend');
set(legs,'TextColor','w','EdgeColor','w','FontSize',16,'color','k');
hold on; box on;
%find errorbars
errorbars=findobj(gcf,'Type','errorbar');
set(errorbars,'LineStyle','none','marker','none','MarkerSize',8,'Color',[0.83,0.82,0.78],'linewidth',2)
set(gcf,'color','k')
set(gca,'FontSize',16,'linewidth',2,'Color','w','Zcolor','w','Xcolor','w','Ycolor','w','color','k')

%find text

txts=findobj(gcf,'Type','text');
for txt=1:numel(txts)
    set(txts(txt),'FontSize',16,'color','w')
end