from mevis import *
import numpy as np
import os
import sys
import matplotlib.pyplot as plt 
import datetime
import pydicom
import subprocess as subp
from subprocess import Popen
global resultlist, allthick, allarea
allarea=np.array([])
allthick = np.array([])

resultlist =np.array([["Structure name","Coordinates needle","Coordinates start-end","Inner contours","Outer contours","mean area","std area","mean thickness","threshold","All areas","All thicknesses","Rough Hausdorff distance"]]).reshape(-1,1)
np.set_printoptions(threshold = 20)


def init():
  ctx.field("Switch.currentInput").setValue(ctx.field("switchval").boolValue())
  ctx.field("WorldVoxelConvert12.voxelX").setValue(40)
  ctx.field("WorldVoxelConvert12.voxelY").setValue(40)
  ctx.field("WorldVoxelConvert12.voxelZ").setValue(40)
  ctx.field("CSOManager4.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager3.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager2.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager1.removeAllCSOsAndGroups").touch()
  ctx.field("DeletesWrongOuterWalls.removeAllCSOsAndGroups").touch()
  ctx.field("DeletesWrongInnerWalls.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager7.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager7.outCSOList").object().addPolyline([[1,1,1],[10,1,1]])
  ctx.field("ImageCache.clear").touch()
  ctx.field("XMarkerListContainer6.deleteAll").touch()
  ctx.field("BoolInt.boolValue").setValue(False)
  ctx.field("SoView2DMarkerEditor2.deleteAll").touch()
  
  
  #ctx.field("DirectDicomImport1.clearVolumeList").touch()

def switchit():
  curval = ctx.field("switchval").boolValue()
  newval= not curval
  ctx.field("switchval").setBoolValue(newval)
  ctx.field("Switch.currentInput").setValue(newval)
  
def removepoints():
  # Resets some fields when the marker points are deleted

  ctx.field("SoView2DMarkerEditor.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor2.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor3.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor4.deleteAll").touch()
  ctx.field("CSOManager3.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager4.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager1.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager2.removeAllCSOsAndGroups").touch() 
  ctx.field("path_reformat.inImage").disconnect()
  ctx.field("enumField").setStringValue("None selected")
  ctx.field("Counter.reset").touch()
  ctx.field("Counter1.reset").touch()
  setstructure()

def removemanualcsopoints():
  ctx.field("SoView2DMarkerEditor.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor4.deleteAll").touch()
    
def storeit():
  # Stores the results of the current walls in a numpy array
  global resultlist,filename
  #get the start and end coordinates
  ctx.field("MarkerListInspector.currentIndex").setIntValue(0)
  startend=np.zeros((3,2))
  startend[...,0]=np.array(ctx.field("MarkerListInspector.currentPositionXYZ").value)
  ctx.field("MarkerListInspector.currentIndex").setIntValue(1)
  startend[...,1]=np.array(ctx.field("MarkerListInspector.currentPositionXYZ").value)
  innercontour = ctx.field("CSOManager4.outCSOList").object()
  outercontour = ctx.field("CSOManager3.outCSOList").object()
  contournrs = innercontour.getNumCSOs()
  innercontournp = np.empty(contournrs,dtype=object)
  outercontournp = np.empty(contournrs,dtype=object)
  pathname=ctx.field("DirectDicomImport1.fileListInfo").stringValue()
  filename=os.path.split(pathname.split('\n')[0])[1][:-4]
  for contindex in range(0,contournrs):
    curinnercontour = np.array(innercontour.getCSOAt(contindex).getPathPoints())
    curoutercontour = np.array(outercontour.getCSOAt(contindex).getPathPoints())
    curinnercontour = np.reshape(curinnercontour,(int(curinnercontour.size/3),3))
    curoutercontour = np.reshape(curoutercontour,(int(curoutercontour.size/3),3))
    outercontournp[contindex]=curoutercontour
    innercontournp[contindex]=curinnercontour
  if not os.path.exists(os.path.join(ctx.field("DirectDicomImport1.fullPath").stringValue(),"Contours")):
    os.makedirs(os.path.join(ctx.field("DirectDicomImport1.fullPath").stringValue(),"Contours")) 
  # save the cso's
  ctx.field("CSOSave.fileName").setStringValue(os.path.join(os.path.join(ctx.field("DirectDicomImport1.fullPath").stringValue(),"Contours","Inner_"+ctx.field("enumField").stringValue()+'_'+filename+'_redone3.cso')))
  ctx.field("CSOSave1.fileName").setStringValue(os.path.join(os.path.join(ctx.field("DirectDicomImport1.fullPath").stringValue(),"Contours","Outer_"+ctx.field("enumField").stringValue()+'_'+filename+'_redone3.cso')))
  ctx.field("CSOSave.startTaskSynchronous").touch()
  ctx.field("CSOSave1.startTaskSynchronous").touch()
  
  startendhold=startend[:]
  resultlist= np.concatenate([resultlist,np.array([ctx.field("enumField").stringValue(),ctx.field("MarkerListInspector1.currentPositionXYZ").vectorValue(),startendhold,innercontournp,outercontournp,ctx.field("meanarea").floatValue(),ctx.field("stdarea").floatValue(),ctx.field("meanthick").floatValue(),ctx.field("setthresh").floatValue(), allarea, allthick,ctx.field("Arithmetic05.resultX").floatValue()])[...,None]],axis=1)
  ctx.field("DirectDicomImport1.fileListInfo").stringValue()
  print(resultlist)
def popit():
  #delete the last column from the results array
  global resultlist
  if not resultlist.size==12:
    resultlist = resultlist[...,:-1]
    print("popped!")
  
  print(resultlist)  
def clearit():
  # clear the entire results array
  global resultlist
  ctx.field("enumField").setStringValue("None selected")
  setstructure()
  resultlist=np.array([["Structure name","Coordinates needle","Coordinates start-end","Inner contours","Outer contours","mean area","std area","mean thickness","threshold","All areas","All thicknesses","Rough Hausdorff distance"]]).reshape(-1,1)
  print('cleared!')
  print(resultlist) 
  
def saveit():
  global filename
  #save the results array
  curtim = datetime.datetime.now().strftime('%Y-%m-%d')
  np.save(os.path.join(os.path.join(ctx.field("DirectDicomImport1.fullPath").stringValue(),"Contour_results_"+filename+'_'+curtim+'additional3.npy')),resultlist)
  print('Results are saved!')
  
def skeletonize():
  ctx.field("BoolInt.boolValue").setValue(False)
  ctx.field("Bypass3.noBypass").setValue(False)
  ctx.field("Bypass3.noBypass").setValue(True)

  # Connect the dtf skeletonization fields and make the shortest path
  ctx.field("prbar").setValue(0)
  ctx.field("DrawVoxels3D1.clear").touch()
  ctx.field("enumField").setStringValue("None selected")
  setstructure()
  if ctx.field("switchval").boolValue()==True:
    ctx.field("IntervalThreshold1.threshMin").setFloatValue(ctx.field("DummyIntervalThreshold.threshMin").floatValue())
  else:
    ctx.field("IntervalThreshold1.threshMin").setFloatValue(ctx.field("OtsuThreshold.threshold").floatValue())
  ctx.field("path_reformat.inImage").connectFrom(ctx.field("Resample3D.output0"))
  ctx.field("XMarkerAtIndex1.update").touch()
  ctx.field("XMarkerAtIndex.update").touch()  
  ctx.field("DtfSkeletonization2.update").touch() 
  ctx.field("SoView2DMarkerEditor.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor4.deleteAll").touch()
  ctx.field("XMarkerShortestPath.updateButton").touch()
  ctx.field("CSOManager3.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager4.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager1.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager2.removeAllCSOsAndGroups").touch()
  ctx.field("BaseBypass.bypass").setValue(True)
  ctx.field("BaseCache.update").touch()
  ctx.field("BaseBypass.bypass").setValue(False)
  ctx.field("prbar").setValue(0.05)
  
def opennormalize():
  window = ctx.showWindow("normalize")
  ctx.field("itkImageFileReader1.close").touch()
  
def bgcorrect():
  
  ANTSdir = '"C:\\Program Files\\ANTs\\Release\\N4BiasFieldCorrection"'
  selectedimage = ctx.field("DirectDicomImport1.fileListInfo").stringValue()
  selectedimage = selectedimage.split('\n')
  selectedimage = selectedimage[0].replace('.dcm','').replace('.nii','')
  if selectedimage[-5:]=='00000':
    selectedimage = selectedimage[:-6]
  if selectedimage[-5:]=='00'+str(ctx.field("Info.sizeZ").intValue()):
    selectedimage = selectedimage[:-6]
  if selectedimage[0]=='*':
    selectedimage = selectedimage[1:]
  if not os.path.isfile(selectedimage + '.nii'): 
    ctx.field("itkImageFileWriter.unresolvedFileName").setStringValue(selectedimage+'.nii')
    ctx.field("itkImageFileWriter.save").touch()
  if not os.path.isfile(selectedimage + '_biascorrected2.nii'):
    ctx.field("itkImageFileWriter1.unresolvedFileName").setStringValue(selectedimage+'_mask.nii')
    ctx.field("itkImageFileWriter1.save").touch()
    ctx.field("itkImageFileWriter2.unresolvedFileName").setStringValue(selectedimage+'_weightmask.nii')
    ctx.field("itkImageFileWriter2.save").touch()
    ctx.field("itkImageFileReader1.unresolvedFileName").setStringValue(selectedimage+'_biascorrected2.nii')
    selectedimagepath, selectedimage = os.path.split(selectedimage)

    systemcall = subp.Popen(["powershell.exe",'cd "'+ selectedimagepath +'/"; docker run --rm -it -v ${pwd}:/kees kaczmarj/ants:2.2.0 N4BiasFieldCorrection ' + ' -i /kees/'+ selectedimage  + '.nii'+' -o /kees/'+ selectedimage +'_biascorrected.nii' + ' -w /kees/'+selectedimage+'_weightmask.nii'+' -x /kees/'+ selectedimage  + '_mask.nii -s 5 -t [0.05] -c [1000x1000x1000x1000,0.000]'])
    systemcall.wait()
    systemcall = subp.Popen(["powershell.exe",'cd "'+ selectedimagepath +'/"; docker run --rm -it -v ${pwd}:/kees kaczmarj/ants:2.2.0 N4BiasFieldCorrection ' + ' -i /kees/'+ selectedimage  + '_biascorrected.nii'+' -o /kees/'+ selectedimage +'_biascorrected2.nii' + ' -w /kees/'+selectedimage+'_weightmask.nii'+' -x /kees/'+ selectedimage  + '_mask.nii -s 5 -t [0.05] -c [1000x1000x1000x1000,0.000]'])
    systemcall.wait()
    
    ctx.field("itkImageFileReader1.open").touch()
    #ctx.field("ImageSave.filename").setStringValue(selectedimage+'_biascorrected2.dcm')
    #ctx.field("ImageSave.save").touch()
    print('finished normalizing background')
  else:
    ctx.field("itkImageFileReader1.unresolvedFileName").setStringValue(selectedimage+'_biascorrected2.nii')
    ctx.field("itkImageFileReader1.open").touch()

def normalize():
  
  agarimage = ctx.field("IntervalThreshold6.output0").image()
  agarimage = agarimage.getTile( (0,0,0), (agarimage.UseImageExtent,agarimage.UseImageExtent,agarimage.UseImageExtent) )
  wallimage = ctx.field("IntervalThreshold7.output0").image()
  wallimage = wallimage.getTile( (0,0,0), (wallimage.UseImageExtent,wallimage.UseImageExtent,wallimage.UseImageExtent) )
  inputimage = ctx.field("itkImageFileReader1.output0").image()
  inputimage = inputimage.getTile((0,0,0), (inputimage.UseImageExtent,inputimage.UseImageExtent,inputimage.UseImageExtent) )
  
  wallmasked = inputimage*wallimage
  agarmasked = inputimage*agarimage
  agarmasked2 = agarmasked[agarmasked!=0]
  wallmasked2 = wallmasked[wallmasked!=0]
  wallmean = np.percentile(wallmasked2,95)
  agarmean = np.median(agarmasked2)

  ctx.field("Arithmetic1.constant").setValue(agarmean)
  ctx.field("Arithmetic11.constant").setValue(wallmean-agarmean)
  ctx.field("itkImageFileWriter5.unresolvedFileName").setStringValue((ctx.field("itkImageFileReader1.unresolvedFileName").stringValue()).replace('biascorrected2','normalized').replace('biascorrected','normalized'))
  if not ctx.field("itkImageFileWriter5.unresolvedFileName").stringValue()==ctx.field("itkImageFileReader1.unresolvedFileName").stringValue():
    ctx.field("itkImageFileWriter5.save").touch()
  else:
    print('Save name is same as input file name. Saving aborted!')

def shide():
  if ctx.field("GVROrthoOverlay.on").value==True:
    ctx.field("GVROrthoOverlay.on").setBoolValue(False)
    ctx.field("GVROrthoOverlay1.on").setBoolValue(False)  
  else:
    ctx.field("GVROrthoOverlay.on").setBoolValue(True)
    ctx.field("GVROrthoOverlay1.on").setBoolValue(True)
    
def manualsegment():
  #Simple switch to set the values of the intervalthreshold3 to the value of the dummy interval threshold
  ctx.field("IntervalThreshold3.threshMin").setStringValue(ctx.field("DummyIntervalThreshold.threshMin").stringValue())

def compute_wall_intens():
  # computes the background median value and foreground median values
  mask = ctx.field("Switch.output0").image()
  mask2 = mask.getTile( (0,0,0), (mask.UseImageExtent,mask.UseImageExtent,mask.UseImageExtent) )
  img = ctx.field("PythonImage1.output0").image()
  img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  masked = mask2*img2
  masked_bg = img2*np.logical_not(mask2)
  ctx.field("prbar").setValue(0.25)
  nonz = masked[masked>0]
  nonz_bg = masked_bg[masked_bg>0]
  medi = np.median(nonz)+np.std(nonz)
  medi_bg = np.median(nonz_bg)
  ctx.field("prbar").setValue(0.8)
  ctx.field("medibg").setValue(medi_bg)
  ctx.field("medi").setValue(medi)
  ctx.field("prbar").setValue(1)
  
def setisovals():
  # sets the isocontour intensity values used for the calculation of the iso contours
  ctx.field("CSOIsoGenerator1.isoValue").setFloatValue(ctx.field("setthresh").value*(ctx.field("medi").value-ctx.field("medibg").value)+ctx.field("medibg").value)
  ctx.field("CSOIsoGenerator2.isoValue").setFloatValue(ctx.field("setthresh").value*(ctx.field("medi").value-ctx.field("medibg").value)+ctx.field("medibg").value)
  ctx.field("CSOIsoGenerator3.isoValue").setFloatValue(ctx.field("setthresh").value*(ctx.field("medi").value-ctx.field("medibg").value)+ctx.field("medibg").value)
  ctx.field("CSOIsoGenerator4.isoValue").setFloatValue(ctx.field("setthresh").value*(ctx.field("medi").value-ctx.field("medibg").value)+ctx.field("medibg").value)  
  
  ctx.field("IntervalThreshold.threshMin").setFloatValue(ctx.field("setthresh").value*(ctx.field("medi").value-ctx.field("medibg").value)+ctx.field("medibg").value)
 
  
def processCSOs():
  #Processes the CSOs
  #Load the image and masks
  #aa= ctx.field("CSOManager2.outCSOList")
  global  allarea, allthick
  img = ctx.field("IntervalThreshold.output0").image()

  ctx.field("enumField").setStringValue("None selected")
  setstructure()
  img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  stats = np.zeros((4,img2.shape[0]))
  ctx.field("CSOManager3.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager4.removeAllCSOsAndGroups").touch()
  ctx.field("DeletesWrongOuterWalls.removeAllCSOsAndGroups").touch()
  ctx.field("DeletesWrongInnerWalls.removeAllCSOsAndGroups").touch()

  innerouterdiffer = np.zeros(img2.shape[0])
  # Generate inner and outer contours
  ctx.field("prbarcontours").setValue(0)
  for slice in range(0,img2.shape[0]):
   
    ctx.field("CSOManager1.removeAllCSOsAndGroups").touch()
    ctx.field("CSOManager2.removeAllCSOsAndGroups").touch()
    hor=np.gradient(img2[slice,int(np.round(img2.shape[1]/2)),:])
    ver=np.gradient(img2[slice,:,int(np.round(img2.shape[2]/2))])

    horis10= np.nonzero(hor)
    veris10= np.nonzero(ver)

    if np.array(horis10).size==0:    
      horis10 = [np.array([np.round(img2.shape[2]/2)-20,np.round(img2.shape[2]/2)+21])]
      print('One or several contours may be incorrectly drawn! Manual correct if necessary. \n')
    if np.array(veris10).size==0:
      veris10 = [np.array([np.round(img2.shape[2]/2)-20,np.round(img2.shape[2]/2)+21])]
      print('One or several contours may be incorrectly drawn! Manual correct if necessary. \n')
      
    if horis10[0].size==0 or veris10[0].size==0:
      print('Something went wrong. Check the mask of the stack. Threshold may be too high! Code execution aborted')
      
    minhor = np.argmin(np.abs(horis10[0]-np.round(img2.shape[1]/2)))
    minver = np.argmin(np.abs(veris10[0]-np.round(img2.shape[2]/2)))
    if np.abs(veris10[0][minver]-np.round(img2.shape[1]/2))<np.abs(horis10[0][minhor]-np.round(img2.shape[2]/2)):
      if horis10[0][minhor]<np.round(img2.shape[1]/2):
        posneg=-1
      else:
        posneg=1
     
      ctx.field("CSOIsoGenerator1.apply").touch()
      
      ctx.field("WorldVoxelConvert2.voxelPos").setStringValue(np.array2string(np.array([horis10[0][minhor],np.round(img2.shape[2]/2),slice]))[1:-1])
      ctx.field("CSOIsoGenerator1.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator1.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      ctx.field("CSOIsoGenerator3.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator3.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())

      outerwall= horis10[0]-horis10[0][minhor]
      if posneg==-1:
        outerwall = outerwall[outerwall<-1]
      else:
        outerwall = outerwall[outerwall>1]  
      
      if outerwall.size==0:
        outerwallcoord = horis10[0][minhor]
      else:
        outerwallcoord = posneg*np.min(np.abs(outerwall))+horis10[0][minhor]
        
      
      ctx.field("WorldVoxelConvert2.voxelPos").setStringValue(np.array2string(np.array([outerwallcoord,np.round(img2.shape[2]/2),slice]))[1:-1])
      ctx.field("CSOIsoGenerator2.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator2.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      ctx.field("CSOIsoGenerator4.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator4.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      
    else:
      if veris10[0][minver]<np.round(img2.shape[2]/2):
        posneg=-1
      else:
        posneg=1
      ctx.field("WorldVoxelConvert2.voxelPos").setStringValue(np.array2string(np.array([np.round(img2.shape[1]/2),veris10[0][minver],slice]))[1:-1])  
      ctx.field("CSOIsoGenerator1.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator3.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator3.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      ctx.field("CSOIsoGenerator1.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      outerwall= veris10[0]-veris10[0][minver]
      if posneg==-1:
        outerwall = outerwall[outerwall<-1]
      else:
        outerwall = outerwall[outerwall>1]  
        
      if outerwall.size==0:
        outerwallcoord = veris10[0][minver]
      else:
        outerwallcoord = posneg*np.min(np.abs(outerwall))+veris10[0][minver]
        
      ctx.field("WorldVoxelConvert2.voxelPos").setStringValue(np.array2string(np.array([np.round(img2.shape[1]/2),outerwallcoord,slice]))[1:-1])
      ctx.field("CSOIsoGenerator2.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator4.startPosition").setStringValue(ctx.field("WorldVoxelConvert2.worldPos").stringValue())
      ctx.field("CSOIsoGenerator4.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      ctx.field("CSOIsoGenerator2.addCSOToGroupWithLabel").setValue(ctx.field("WorldVoxelConvert2.voxelZ").intValue())
      
    ctx.field("CSOIsoGenerator1.apply").touch()
    ctx.field("CSOIsoGenerator2.apply").touch()
    ctx.field("CSOIsoGenerator3.apply").touch()
    ctx.field("CSOIsoGenerator4.apply").touch()
    ctx.field("prbarcontours").setValue((slice+1)/img2.shape[0])



  #regionresult = ctx.field("RegionGrowing.output0").image()
  #regionresult2 = regionresult.getTile( (0,0,0), (regionresult.UseImageExtent,regionresult.UseImageExtent,regionresult.UseImageExtent) )
  #areas = np.sum(np.sum(regionresult2,axis=2),axis=1)
  #vsize = np.fromstring(ctx.field("Resample3D.voxelSize").stringValue(),sep=' ')
  #mm2areas = vsize[0]*vsize[1]*areas
  #print('mean wall area (mm2):')
  #switch on and off to send the csos through to the next list, which will delete the wrong ones.
  ctx.field("DeletesWrongInnerWalls.workDirectlyOnInputCSOList").setBoolValue(True)
  ctx.field("DeletesWrongOuterWalls.workDirectlyOnInputCSOList").setBoolValue(True)
  ctx.field("DeletesWrongOuterWalls.workDirectlyOnInputCSOList").setBoolValue(False)
  ctx.field("DeletesWrongInnerWalls.workDirectlyOnInputCSOList").setBoolValue(False)
  ctx.field("CSOResample.apply").touch()
  ctx.field("CSOResample1.apply").touch()
  compute_measures()
  
def select_deselect():
  if ctx.field("CSOInfo.numCSOs").value>0:
    ctx.field("DeletesWrongInnerWalls.workDirectlyOnInputCSOList").setBoolValue(True)
    ctx.field("DeletesWrongOuterWalls.workDirectlyOnInputCSOList").setBoolValue(True)
    ctx.field("DeletesWrongOuterWalls.workDirectlyOnInputCSOList").setBoolValue(False)
    ctx.field("DeletesWrongInnerWalls.workDirectlyOnInputCSOList").setBoolValue(False)
    
    selectedcsos = np.array([-1000])  
    unselectedcsos = np.array([-1000])
    if ctx.field("XMarkerListContainer.numItems").value>0:
      for greenind in range(0,ctx.field("XMarkerListContainer.numItems").value):
        ctx.field("XMarkerListContainer.index").setValue(greenind)
        selectedcsos = np.append(selectedcsos,int(ctx.field("WorldVoxelConvert7.voxelZ").value))
    
    if ctx.field("XMarkerListContainer1.numItems").value>0:
      for redind in range(0,ctx.field("XMarkerListContainer1.numItems").value):
        ctx.field("XMarkerListContainer1.index").setValue(redind)
        unselectedcsos = np.append(unselectedcsos,ctx.field("WorldVoxelConvert8.voxelZ").value) 
    img = ctx.field("IntervalThreshold.output0").image()
    img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )    
    outerlist = ctx.field("CSOManager4.outCSOList").object()
    innerlist = ctx.field("CSOManager3.outCSOList").object()
  
    for slice in range(0,img2.shape[0]):
      inner = innerlist.getGroupById(slice+1).getCSOAt(0)
      outer = outerlist.getGroupById(slice+1).getCSOAt(0)
      if np.any(unselectedcsos==slice):
        inner.setColor((1,0,0))
        outer.setColor((1,0,0))
        ctx.field("DeletesWrongOuterWalls.csoSelectedItems").setValue(ctx.field("DeletesWrongOuterWalls.outCSOList").object().getGroupAt(slice).getCSOAt(0).getId())
        ctx.field("DeletesWrongInnerWalls.csoSelectedItems").setValue(ctx.field("DeletesWrongInnerWalls.outCSOList").object().getGroupAt(slice).getCSOAt(0).getId())
        ctx.field("DeletesWrongOuterWalls.csoRemoveSelected").touch()
        ctx.field("DeletesWrongInnerWalls.csoRemoveSelected").touch()
      elif np.any(selectedcsos==slice):
        inner.setColor((0,1,0))
        outer.setColor((0,1,0))
      elif  inner.getColor()==(1,0,0):
        ctx.field("DeletesWrongOuterWalls.csoSelectedItems").setValue(ctx.field("DeletesWrongOuterWalls.outCSOList").object().getGroupAt(slice).getCSOAt(0).getId())
        ctx.field("DeletesWrongInnerWalls.csoSelectedItems").setValue(ctx.field("DeletesWrongInnerWalls.outCSOList").object().getGroupAt(slice).getCSOAt(0).getId())
        ctx.field("DeletesWrongOuterWalls.csoRemoveSelected").touch()
        ctx.field("DeletesWrongInnerWalls.csoRemoveSelected").touch()      
        
def compute_measures():
  global  allarea, allthick
  
  img = ctx.field("IntervalThreshold.output0").image()
  img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  innerouterdiffer = np.zeros(img2.shape[0])
  stats = np.zeros((4,img2.shape[0]))
  # Check if slices have been added or deleted manually
  selectedcsos = np.array([-1000])  
  unselectedcsos = np.array([-1000])
  if ctx.field("XMarkerListContainer.numItems").value>0:
    for greenind in range(0,ctx.field("XMarkerListContainer.numItems").value):
      ctx.field("XMarkerListContainer.index").setValue(greenind)
      selectedcsos = np.append(selectedcsos,int(ctx.field("WorldVoxelConvert7.voxelZ").value))
  
  if ctx.field("XMarkerListContainer1.numItems").value>0:
    for redind in range(0,ctx.field("XMarkerListContainer1.numItems").value):
      ctx.field("XMarkerListContainer1.index").setValue(redind)
      unselectedcsos = np.append(unselectedcsos,ctx.field("WorldVoxelConvert8.voxelZ").value) 
      
  for slice in range(0,img2.shape[0]):
    ctx.field("CSOInfo3.groupShowByIndexOrId").setValue(slice)
    ctx.field("CSOInfo.groupShowByIndexOrId").setValue(slice)
    
    if np.abs(ctx.field("CSOInfo.groupArea").floatValue()-ctx.field("CSOInfo3.groupArea").floatValue())<1e-2:
      stats[0,slice] = 0
      stats[2,slice] = 0   
      stats[1,slice] = ctx.field("CSOInfo3.groupLength").floatValue()
      stats[3,slice] = ctx.field("CSOInfo3.groupArea").floatValue()     
    else:
      innerouterdiffer[slice]=1
      stats[0,slice] = ctx.field("CSOInfo.groupLength").floatValue()     
      stats[2,slice] = ctx.field("CSOInfo.groupArea").floatValue()      
      stats[1,slice] = ctx.field("CSOInfo3.groupLength").floatValue()     
      stats[3,slice] = ctx.field("CSOInfo3.groupArea").floatValue()      
      
    areas = stats[3,...]-stats[2,...]
  # use the complete list, including wrong slices, to estimate the mean areas and thicknesses
  outerlist = ctx.field("CSOManager4.outCSOList").object()
  innerlist = ctx.field("CSOManager3.outCSOList").object()
  outerwallsubgroup = ctx.field("DeletesWrongOuterWalls.outCSOList").object()
  innerwallsubgroup = ctx.field("DeletesWrongInnerWalls.outCSOList").object()
  chlist = np.zeros((img2.shape[0]))
  for contour in range(0,img2.shape[0]):
    # piece of code below, checks the hausdorff distance from inner to outer and vice versa, and sees whether some points are outliers, which might indicate a different vessel wall close by.
    inner = innerlist.getGroupById(contour+1).getCSOAt(0)
    innercont = np.array([inner.getSeedPoints()])
    innercont = innercont.reshape(int(innercont.size/3),3)
    outer = outerlist.getGroupById(contour+1).getCSOAt(0)
    outercont = np.array([outer.getSeedPoints()])
    outercont = outercont.reshape(int(outercont.size/3),3)

    outercont2 = np.tile(outercont,(innercont.shape[0],1,1))

    innercont3 = np.tile(np.transpose(innercont),(outercont.shape[0],1,1))
    innercont3 = np.transpose(innercont3,[2,0,1]) 
    multo=outercont2-innercont3
    multo = np.power(multo,2)
    multo2=np.sum(multo,axis=2) 
    min2 = np.argmin(multo2,axis=1)
    min1 = np.argmin(multo2,axis=0)
    outertoinner = multo2[min1,np.linspace(0,multo2.shape[1]-1,multo2.shape[1]).astype('int')]
    innertoouter = multo2[np.linspace(0,multo2.shape[0]-1,multo2.shape[0]).astype('int'),min2]
    meaninner = np.mean(innertoouter)
    stdinner = np.std(innertoouter)
    meanouter = np.mean(outertoinner)
    if (meanouter<(meaninner+stdinner) or np.any(selectedcsos==contour)) and not np.any(unselectedcsos==contour) and not stats[0,contour]==0 :
       chlist[contour]=1
       inner.setColor((0,1,0))
       outer.setColor((0,1,0))
    elif meanouter>(meaninner+stdinner) or np.any(unselectedcsos==contour) or np.abs(meanouter-meaninner)<0.0001:
       inner.setColor((1,0,0))
       outer.setColor((1,0,0))
       ctx.field("DeletesWrongOuterWalls.csoSelectedItems").setValue(outerwallsubgroup.getGroupAt(contour).getCSOAt(0).getId())
       ctx.field("DeletesWrongInnerWalls.csoSelectedItems").setValue(innerwallsubgroup.getGroupAt(contour).getCSOAt(0).getId())
       ctx.field("DeletesWrongOuterWalls.csoRemoveSelected").touch()
       ctx.field("DeletesWrongInnerWalls.csoRemoveSelected").touch()
    else:
       chlist[contour]=1
       inner.setColor((0,1,0))
       outer.setColor((0,1,0))      

  ctx.field("meanarea").setFloatValue(np.mean(areas[chlist==1]))
  ctx.field("stdarea").setFloatValue(np.std(areas[chlist==1]))
  allarea = areas[chlist==1]
  allthick = np.sqrt(stats[3,chlist==1]/np.pi)-np.sqrt(stats[2,chlist==1]/np.pi)
  ctx.field("meanthick").setFloatValue(np.mean(np.sqrt(stats[3,(chlist==1) & (innerouterdiffer==1)]/np.pi)-np.sqrt(stats[2,(chlist==1) & (innerouterdiffer==1)]/np.pi)))

  print('mean area (mm2):'+str(np.mean(areas[chlist==1]))+'+-'+np.str(np.std(areas[chlist==1])))      
  print('mean thickness (mm):'+ str(np.mean(np.sqrt(stats[3,(chlist==1) & (innerouterdiffer==1)]/np.pi)-np.sqrt(stats[2,(chlist==1) & (innerouterdiffer==1)]/np.pi))))
  
#  control = ctx.control("canvas").object()
 # control.figure().clear()
  # clear the user interaction of plotC example:
 # control.myUserInteraction = None
  
 # figure = ctx.control("canvas").object().figure()
 # subplot = figure.add_subplot(111)
 # subplot.plot(areas)

#  subplot.set_xlabel('slice') 
#  subplot.set_ylabel('Area (mm2)') 
 # subplot.set_title('Areas') 
#  figure.canvas.draw()


def setstructure():
  #Sets the overlays on the circle of willis image to the selected structure
  if ctx.field("enumField").stringValue()=="None selected":
    ctx.field("DrawRectangle.start").setStringValue('-100 -100')
  if ctx.field("enumField").stringValue()=="D1":
    ctx.field("DrawRectangle.start").setStringValue('447 92')
  if ctx.field("enumField").stringValue()=="D2":
    ctx.field("DrawRectangle.start").setStringValue('589 75')
  if ctx.field("enumField").stringValue()=="C3":
    ctx.field("DrawRectangle.start").setStringValue('649 195')
  if ctx.field("enumField").stringValue()=="C6":
    ctx.field("DrawRectangle.start").setStringValue('694 136') 
  if ctx.field("enumField").stringValue()=="C4":
    ctx.field("DrawRectangle.start").setStringValue('811 214')
  if ctx.field("enumField").stringValue()=="B5":
    ctx.field("DrawRectangle.start").setStringValue('725 260')  
  if ctx.field("enumField").stringValue()=="B3":
    ctx.field("DrawRectangle.start").setStringValue('731 332') 
  if ctx.field("enumField").stringValue()=="D4":
    ctx.field("DrawRectangle.start").setStringValue('708 390')  
  if ctx.field("enumField").stringValue()=="B1":
    ctx.field("DrawRectangle.start").setStringValue('577 327')
  if ctx.field("enumField").stringValue()=="B2":
    ctx.field("DrawRectangle.start").setStringValue('435 350')  
  if ctx.field("enumField").stringValue()=="D3":
    ctx.field("DrawRectangle.start").setStringValue('458 416')  
  if ctx.field("enumField").stringValue()=="B4":
    ctx.field("DrawRectangle.start").setStringValue('387 283') 
  if ctx.field("enumField").stringValue()=="C5":
    ctx.field("DrawRectangle.start").setStringValue('375 173')
  if ctx.field("enumField").stringValue()=="C1":
    ctx.field("DrawRectangle.start").setStringValue('429 231')
  if ctx.field("enumField").stringValue()=="C2":
    ctx.field("DrawRectangle.start").setStringValue('257 266')  
  if ctx.field("enumField").stringValue()=="A6":
    ctx.field("DrawRectangle.start").setStringValue('590 477')
  if ctx.field("enumField").stringValue()=="A3":
    ctx.field("DrawRectangle.start").setStringValue('578 575')
  if ctx.field("enumField").stringValue()=="A4":
    ctx.field("DrawRectangle.start").setStringValue('469 590') 
  if ctx.field("enumField").stringValue()=="A5":
    ctx.field("DrawRectangle.start").setStringValue('698 579')
  if ctx.field("enumField").stringValue()=="A1":
    ctx.field("DrawRectangle.start").setStringValue('447 769')  
  if ctx.field("enumField").stringValue()=="A2":
    ctx.field("DrawRectangle.start").setStringValue('712 769')
  
def roisel():
  window = ctx.showWindow("roisel2")
def showMIP():
  window = ctx.showWindow("showmip")
def openexperimental():
  if ctx.control("experimentalbox").isVisible()==0:
    ctx.control("experimentalbox").setVisible(1)
    ctx.control("openinner").setVisible(1)
    ctx.control("openouter").setVisible(1)
    ctx.control("deletedrawn").setVisible(1)
  #  ctx.control("applyinnercontours").setVisible(1)
  else:
    ctx.control("experimentalbox").setVisible(0)
    ctx.control("openinner").setVisible(1)
    ctx.control("openouter").setVisible(1)
    ctx.control("deletedrawn").setVisible(1)
   # ctx.control("applyinnercontours").setVisible(1)

    
def openinner():
  ctx.module("CSOManager7").showDefaultPanel()

def openouter():
    ctx.module("CSOManager8").showDefaultPanel()
    
def deletedrawncontours():
  ctx.field("CSOManager7.removeAllCSOsAndGroups").touch()
  ctx.field("CSOManager8.removeAllCSOsAndGroups").touch()
  
def applyinnercontours():
  #ctx.field("CSOManager3.copyInputCSOList").touch()
  #ctx.field("CSOManager4.copyInputCSOList").touch()
  
  #Listens if a new CSO is drawn, or an existing one is edited. Will update the CSOtoimage module, so velocity values across the newly drawn/edited CSO contour are used.
  if ctx.field("CSOManager3.numCSOs").value > ctx.field("CSOManager3.numGroups").value:
    
    csolist = ctx.field("CSOManager3.outCSOList").object()
    csogrouplist = csolist.getGroupByLabel(ctx.field("Arithmetic06.resultX").intValue())
    csolist.removeCSO(csogrouplist.getCSOIdAt(0))  
    lastid = ctx.field("CSOManager3.outCSOList").object().getCSOIdList()[-1]
    ctx.field("CSOSmooth.csoIdList").setValue(str(lastid))
    ctx.field("CSOSmooth.apply").touch()
    ctx.field("CSOResample.apply").touch()
    select_deselect()
def applyoutercontours():
  #ctx.field("CSOManager3.copyInputCSOList").touch()
  #ctx.field("CSOManager4.copyInputCSOList").touch()
  
  #Listens if a new CSO is drawn, or an existing one is edited. Will update the CSOtoimage module, so velocity values across the newly drawn/edited CSO contour are used.
  if ctx.field("CSOManager4.numCSOs").value > ctx.field("CSOManager4.numGroups").value:
    csolist = ctx.field("CSOManager4.outCSOList").object()
    csogrouplist = csolist.getGroupByLabel(ctx.field("Arithmetic06.resultX").intValue())
    csolist.removeCSO(csogrouplist.getCSOIdAt(0))  
    lastid = ctx.field("CSOManager4.outCSOList").object().getCSOIdList()[-1]
    ctx.field("CSOSmooth1.csoIdList").setValue(str(lastid))
    ctx.field("CSOResample1.apply").touch()
    ctx.field("CSOSmooth1.apply").touch()
    if 0:
      ctx.field("DeletesWrongOuterWalls.outCSOList").object().addCSOCopy(ctx.field("CSOManager4.outCSOList").object().getGroupById(ctx.field("Arithmetic06.resultX").intValue()+1).getCSOAt(0))
      csolist = ctx.field("DeletesWrongOuterWalls.outCSOList").object()
      csogrouplist = csolist.getGroupByLabel(ctx.field("Arithmetic06.resultX").intValue())
      csolist.removeCSO(csogrouplist.getCSOAt(0))  
    select_deselect()
    
def getdirection():
  if ctx.field("Counter1.currentValue").value==2:
    imgsize = np.ceil(ctx.field("Info1.sizeX").value/2)
    
    agarimage = ctx.field("Arithmetic12.output0").image()
    agarimage = agarimage.getTile( (0,0,0), (agarimage.UseImageExtent,agarimage.UseImageExtent,agarimage.UseImageExtent) )
    kernsize = 3
    small=agarimage[imgsize-kernsize:imgsize+kernsize,imgsize-kernsize:imgsize+kernsize,imgsize-kernsize:imgsize+kernsize]
    ag=small[np.where(small!=agarimage[imgsize,imgsize,imgsize])]
    minval = np.min(ag)
    ar = np.array(np.where(small==minval))
    ctx.field("ComposeVector3.x").setValue(ar[0][0]-kernsize)
    ctx.field("ComposeVector3.y").setValue(ar[1][0]-kernsize)
    ctx.field("ComposeVector3.z").setValue(ar[2][0]-kernsize)
    ctx.field("XMarkerShortestPath1.updateButton").touch()
    
def dodtf():
  if ctx.field("Counter.currentValue").value==2:
    ctx.field("Counter.reset").touch()
    ctx.field("XMarkerListContainer2.add").touch()
    ctx.field("XMarkerListCompare.update").touch()

    if ctx.field("XMarkerListCompare.outAreEqual").boolValue()!=True:
      ctx.field("DtfSkeletonization1.update").touch()

def compute_thickness_per_point():
  global csoList
  ctx.field("XMarkerListContainer3.deleteAll").touch()
  ctx.field("CSOConvertToImage2.apply").touch()
  ctx.field("CSOConvertToImage3.apply").touch()
  ctx.field("EuclideanDistanceTransform.update").touch()
  skeleton = ctx.field("IntervalThreshold13.output0").image()
  skeleton = skeleton.getTile( (0,0,0), (skeleton.UseImageExtent,skeleton.UseImageExtent,skeleton.UseImageExtent) )
  nonzero_locations = np.nonzero(skeleton)
  nonzero_locations = np.array([nonzero_locations[2] ,nonzero_locations[1] ,nonzero_locations[0] ])
  cso_innercontour = ctx.field("DeletesWrongInnerWalls.outCSOList").object()
  thicknessmap = np.zeros(skeleton.shape).astype('float32')
  print(np.count_nonzero(skeleton))
  csoList = ctx.field("DeletesWrongOuterWalls.outCSOList").object().getCSOGroups()
  for point in range(0,np.count_nonzero(skeleton)):
    current_location = nonzero_locations[:,point]
    #print(current_location)
    ctx.field("WorldVoxelConvert11.voxelPos").setValue(current_location)
    #ctx.field("VectorLine1.start").setValue(current_location)
    current_world_location = np.array([ctx.field("WorldVoxelConvert11.worldPos").value])
    
    cso = cso_innercontour.getCSOGroups()[current_location[-1]].getCSOAt(0)
    pathPoints = cso.getPathPointsAsNumPyArray()
    closest_inner_point = pathPoints[np.argmin(np.sum(np.square(pathPoints-current_world_location),axis=1),axis=None)]

    extrapolated_line = closest_inner_point + 5 * (current_world_location - closest_inner_point)

    set_line_through_point(closest_inner_point,extrapolated_line[0],point)
   # reinterpolate_path_points()
    location_outer = get_intersection(current_location)
    #print(current_location[0])
    #print(np.sqrt(np.sum(np.square(location_outer-closest_inner_point))))
    thicknessmap[current_location[2],current_location[1],current_location[0]]= np.sqrt(np.sum(np.square(location_outer-closest_inner_point)))
    
  interface = ctx.module("Thicknessmap").call("getInterface")
  interface.setImage(thicknessmap, minMaxValues =(np.min(thicknessmap),np.max(thicknessmap)))
  MPR_to_original([])
    #ctx.field("WorldVoxelConvert11.worldPos").setValue(closest_inner_point)
    #ctx.field("VectorLine1.end").setValue(ctx.field("WorldVoxelConvert11.voxelPos").value)
  
def set_line_through_point(start,end,index):
  line=ctx.field("CSOManager7.outCSOList").object()
  line_cso=line.getCSOAt(0)
  #print(end)
  line_cso.setSeedPointPositionAt(1,end)
  line_cso.setSeedPointPositionAt(0,start)
  line_cso.recomputePathPointsLinear() 
  ctx.field("CSOResample2.apply").touch()

  
def reinterpolate_path_points():
  global reinterpolated_path_points
  reinterpolated_path_points = np.zeros((3,ctx.field("XMarkerListContainer4.numItems").value))
  xmarkerlist = ctx.field("XMarkerListContainer4.outXMarkerList").object().getMarkers()
  for marker in range(0,ctx.field("XMarkerListContainer4.numItems").value):
    
    reinterpolated_path_points[:,marker]=  [xmarkerlist[marker].x,xmarkerlist[marker].y,xmarkerlist[marker].z]

def get_intersection(current_location):
  global csoList
  
  cso2 = ctx.field("CSOManager8.outCSOList").object().getCSOAt(0)
  cso = csoList[current_location[-1]].getCSOAt(0)
  pathPoints = cso.getPathPointsAsNumPyArray()
  pathPoints2 = np.transpose(cso2.getPathPointsAsNumPyArray())
  #print(pathPoints)
 # pathPoints2 = np.reshape(np.array(cso2.getPathPoints()),(cso2.getNumUniquePathPoints(),3))
 # print(pathPoints2)
  tilepath = np.tile(pathPoints[...,None],(1,1,pathPoints2.shape[1]))
  #tilepath2 = np.tile(pathPoints2[...,None],(1,1,pathPoints.shape[0]))
  #tilepath2 = np.transpose(tilepath2,(2,1,0))

  ind= np.unravel_index(np.argmin(np.sum(np.square(tilepath-pathPoints2),axis=1),axis=None),(tilepath.shape[0],tilepath.shape[2]))
  #print(ind)
  location = pathPoints2[:,ind[1]]
  if 1:
    ctx.field("XMarkerListContainer3.add").touch()
    ctx.field("XMarkerListContainer3.posXYZ").value = location
  return location

def set_thicknessmap_image():
  
  if ctx.field("BoolInt.boolValue").value==True:
    img = ctx.field("ImageCache2.output0").image()
  else:
    img = ctx.field("DirectDicomImport1.output0").image()
  img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) ) 
  thicknessmap_original = np.zeros(img2.shape)
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(thicknessmap_original)
  init()
  

def MPR_to_original(mask):
  
  img = ctx.field("ImageCache2.output0").image()
  img2 = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )

  thicknessmap_original = np.zeros(img2.shape)
  thicknessmap_MPR = ctx.field("Thicknessmap.output0").image()
  thicknessmap_MPR = np.transpose(thicknessmap_MPR.getTile( (0,0,0), (thicknessmap_MPR.UseImageExtent,thicknessmap_MPR.UseImageExtent,thicknessmap_MPR.UseImageExtent) ),(1,2,0))
  if np.array(mask).size==0:
    mask = np.ones(thicknessmap_MPR.shape)
  thicknessmap_MPR = thicknessmap_MPR * mask
    
  print(thicknessmap_MPR.shape)
  
  for slice in range(0,ctx.field("MPRPath1.maxKeyFrame").value+1):
    
    ctx.field("MPRPath1.currentKeyFrame").setValue(slice)
    currentslice = thicknessmap_MPR[...,slice]
    nonzero_locations = np.nonzero(currentslice)
    if nonzero_locations[0].size!=0:
      nonzero_locations = np.array([nonzero_locations[1] ,nonzero_locations[0] ,np.zeros(nonzero_locations[0].size),np.ones(nonzero_locations[0].size) ])
      nonzero_values = currentslice[np.nonzero(currentslice)]
      transformmatrix= np.array(ctx.field("MatrixArithmetic.outputMatrixC").value)
      original_locations = transformmatrix @ nonzero_locations
      if np.array(original_locations.shape).size==1:
        original_locations = original_locations[...,None]
        
      rows, cols,slices = zip(original_locations[:-1,...].astype(int))
      thicknessmap_original[slices,cols,rows]=nonzero_values
      
  interface = ctx.module("Thicknessmap_original_space").call("getInterface")
  interface.setImage(thicknessmap_original, minMaxValues =(np.min(thicknessmap_original),np.max(thicknessmap_original)))
  
  
def merge_thicknessmap():
  ctx.field("Bypass1.noBypass").setValue(False)
  thicknessmap = ctx.field("Arithmetic21.output0").image()
  thicknessmap = thicknessmap.getTile( (0,0,0), (thicknessmap.UseImageExtent,thicknessmap.UseImageExtent,thicknessmap.UseImageExtent) )
  ctx.field("Bypass1.noBypass").setValue(True)
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(thicknessmap, minMaxValues =(np.min(thicknessmap),np.max(thicknessmap)))  
  
def innercsolistener(): 
  #Listens if a new CSO is drawn, or an existing one is edited. Will update the CSOtoimage module, so velocity values across the newly drawn/edited CSO contour are used.
  if ctx.field("CSOManager3.numCSOs").value > ctx.field("CSOManager3.numGroups").value:
    
    csolist = ctx.field("CSOManager3.outCSOList").object()
    csogrouplist = csolist.getGroupByLabel(ctx.field("Arithmetic06.resultX").intValue())
    csolist.removeCSO(csogrouplist.getCSOIdAt(0))  
  select_deselect()
    
def outercsolistener(): 
  #Listens if a new CSO is drawn, or an existing one is edited. Will update the CSOtoimage module, so velocity values across the newly drawn/edited CSO contour are used.
  if ctx.field("CSOManager4.numCSOs").value > ctx.field("CSOManager4.numGroups").value:
    csolist = ctx.field("CSOManager4.outCSOList").object()
    csogrouplist = csolist.getGroupByLabel(ctx.field("Arithmetic06.resultX").intValue())
    csolist.removeCSO(csogrouplist.getCSOIdAt(0))  
  select_deselect()
  
def apply_edited_mask():
  mask = ctx.field("ImageSwitch1.output0").image()
  mask = mask.getTile( (0,0,0), (mask.UseImageExtent,mask.UseImageExtent,mask.UseImageExtent) )
  mask = np.transpose(mask,(1,2,0))
  MPR_to_original(mask)
  
def turn_bypass():
  if ctx.field("currenttab").value>0:
    ctx.field("Bypass2.noBypass").setValue(False)
  else:
    ctx.field("Bypass2.noBypass").setValue(True)
  if ctx.field("currenttab").value==1:
    initial_masking()
    
def initial_masking():  
  agarimage = ctx.field("ImagePropertyConvert.input0").image()
  agarimage = agarimage.getTile( (0,0,0), (agarimage.UseImageExtent,agarimage.UseImageExtent,agarimage.UseImageExtent) )
  mask = agarimage>10
  agarimage = agarimage*mask
  interface = ctx.module("PythonImage1").call("getInterface")
  interface.setImage(agarimage, minMaxValues =(np.min(agarimage),np.max(agarimage)))
  
